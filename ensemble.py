import random

import numpy as np
from sklearn.linear_model import Ridge

import bayeserror
import cstats
import learning
import util


class BNEnsemble():
    '''
    A class to represent an ensemble of Bayesian Networks.
    '''

    def __init__(self, data=None, nNetworks=10, nAttr=None, bagSize=None,
                 alg='MMHC', models=None):
        '''
        Initialize an ensemble of Bayesian networks. If a database is given,
        an ensemble will be learned with nNetworks networks with nAttr
        attributes per network. Bagging is used to create a dataset for
        training each individual network. Each bagged dataset contains bagSize
        instances sampled from the original database with replacement. A
        collection of models that have already been learned can be passed in
        via the models parameter.

        data : A TableDB object containing data to be used for training the
            ensemble.
        nNetworks : The number of networks to learn.
        nAttr : The number of attributes to use for each network. If this value
            is less than the number of variables in the database, a random set
            of variables is chosen.
        bagSize : The size of each bagged dataset to be used for training an
            individual network.
        alg : The algorithm to use for learning each individual network. Valid
            arguments are 'FAS' for Fast Adjacency Search, 'HC' for hill
            climbing search, or 'MMHC' for Max-Min Hill Climbing.
        models : A collection of already-learned networks to be added to the
            ensemble.
        '''

        if models is None:
            models = []

        self._models = list(models)

        if alg == 'FAS':
            learn = learning.FAS
        elif alg == 'HC':
            learn = learning.hillClimbing
        elif alg == 'MMHC':
            learn = learning.MMHC
        else:
            raise ValueError('\'{}\' is not a valid learning algorithm'
                             .format(alg))

        if data is not None:
            for i in range(nNetworks):
                bag = data.bag(nAttr, bagSize)
                network = learn(bag)
                learning.estimateParams(network, bag)
                self._models.append(network)

    def __len__(self):
        '''
        Get the number of models in the ensemble.
        '''

        return len(self._models)

    def __iter__(self):
        '''
        Iterate over the models in the ensemble.
        '''

        return iter(self._models)


class LGNEnsemble():
    def __init__(self, data=None, nNetworks=10, k=1, emItr=10,
                 dataWeights=None, priors=None, nAttr=None, testAttr=None,
                 bagSize=None, alg='MMHC', models=None):

        if models is None:
            models = []

        if nAttr is None:
            nAttr = len(data.variables)

        self._models = list(models)
        self._gmm = None
        self._weights = np.ones(nNetworks)

        if alg == 'FAS':
            learn = learning.FAS
        elif alg == 'HC':
            learn = learning.hillClimbing
        elif alg == 'MMHC':
            learn = learning.MMHC
        else:
            raise ValueError('\'{}\' is not a valid learning algorithm'
                             .format(alg))

        if data is not None:
            # Dict to store data weights for subsets of variables
            emWeights = {}

            for i in range(nNetworks):
                # Select variables for learning (randomization)
                if testAttr is not None:
                    trainAttr = random.sample(set(data.variables) -
                                              set(testAttr), nAttr - 1)
                    attr = trainAttr + list(testAttr)
                else:
                    attr = random.sample(set(data.variables), nAttr)
                attr = tuple(sorted(attr))

                if attr not in emWeights:
                    # Perform EM to assign weights to instances for bagging
                    subdata = data.subdatabase(attr)
                    dataWeights, priors = \
                        self._computeDataWeights(subdata, k, emItr)
                    emWeights[attr] = (dataWeights, priors)
                else:
                    dataWeights, priors = emWeights[attr]

                # Perform weighted bagging and learn a network
                while True:
                    try:
                        weights = util.choice(dataWeights, priors)
                        bag = data.bagPSD(attr, bagSize, weights, maxTries=100)
                        network = learn(bag)
                        learning.estimateParams(network, bag)
                        self._models.append(network)
                        break
                    except bayeserror.MaxTriesExceeded as e:
                        # Rerun EM if weighted bagging fails
                        print(e)
                        dataWeights, priors = \
                            self._computeDataWeights(subdata, k, emItr)
                        emWeights[attr] = (dataWeights, priors)

            if testAttr is not None:
                # Compute component weights (improves randomization accuracy)
                trainAttr = set(data.variables) - set(testAttr)
                self._weights = self._computeComponentWeights(data, trainAttr,
                                                              testAttr)

    def _computeDataWeights(self, data, k, emItr):
        '''
        Perform EM to assign weights to each data instance.
        '''

        while True:
            try:
                means, covs, priors, dataWeights = (
                    learning.gaussianEM(data, k, emItr))
                return dataWeights, priors
            except ValueError as e:
                print('Retrying EM for {}... non-PD covariance '
                      '({} samples)'.format(data.variables, len(data)))

    @util.logged
    def _computeComponentWeights(self, data, trainAttr, testAttr):
        '''
        Perform regression to assign a fixed weight to each component of the
        ensemble. This should improve prediction accuracy when using
        randomization (see section 3.1 of Hellman).
        '''

        # Compute prediction for each model on each instance
        train = data.subdatabase(trainAttr)
        target = data.subdatabase(testAttr)
        predictions = np.zeros((len(data), len(self)))
        likelihoods = np.zeros((len(data), len(self)))

        for i, inst in enumerate(train.instances()):
            for j, dist in enumerate(self.toGMM()):
                reducedInst = {v: val for v, val in inst.items()
                               if v in dist.variables}
                predictions[i, j] = dist.inference(testAttr, reducedInst).mean
                likelihoods[i, j] = (dist.partition(reducedInst.keys()).
                                     pdf(reducedInst))

        # Weight predictions by likelihoods
        predictions = ((predictions * likelihoods).T /
                       likelihoods.sum(axis=1)).T

        # NaNs may have been produced in the previous calculation for instances
        # whose likelihood is zero for all ensemble components. The weights for
        # these instances should be zero.
        predictions = np.nan_to_num(predictions)

        # Use ridge regression to compute component weights
        ridge = Ridge(fit_intercept=False)
        ridge.fit(predictions, target.data)
        weights = ridge.coef_.flatten()
        #print('rr weights:\t{}'.format(weights))
        return weights

    def __len__(self):
        '''
        Get the number of models in the ensemble.
        '''

        return len(self._models)

    def __iter__(self):
        '''
        Iterate over the models in the ensemble.
        '''

        return iter(self._models)

    def toGMM(self):
        '''
        Convert the ensemble to a list of Gaussian distributions.
        '''

        if self._gmm is None:
            self._gmm = [cstats.Gaussian(network=lgn, varNames=lgn.nodes()) for
                         lgn in self._models]

        return self._gmm

    def inference(self, query, inst):
        prediction = 0
        weightSum = 0

        for dist, weight in zip(self.toGMM(), self._weights):
            nodes = set(dist.variables)
            reducedQuery = set(query) & set(nodes)
            reducedInst = {node: val for node, val in inst.items()
                           if node in nodes}

            # Perform inference using the equivalent multivariate Gaussian
            # distribution
            p = dist.partition(reducedInst.keys()).pdf(reducedInst)
            result = dist.inference(reducedQuery, reducedInst)

            prediction += p * result.mean * weight
            weightSum += p

        # Return 0 if no one can make a decent prediction
        if weightSum == 0:
            return 0

        return prediction / weightSum


class DynamicLGNEnsemble(LGNEnsemble):
    def __init__(self, slice0, slice1, nNetworks=10, k=1, emItr=10,
                 nAttr=None, testAttr=None, bagSize=None, alg='MMHC',
                 models=None):

        if models is None:
            models = []

        if nAttr is None:
            nAttr = len(slice0.variables)

        self._models = list(models)
        self._gmm = None
        self._weights = np.ones(nNetworks)

        if alg == 'FAS':
            learn = learning.FAS
        elif alg == 'HC':
            learn = learning.hillClimbing
        elif alg == 'MMHC':
            learn = learning.MMHC
        else:
            raise ValueError('\'{}\' is not a valid learning algorithm'
                             .format(alg))

        # Create database for dynamic networks
        assert(slice0.variables == slice1.variables)
        slice0Vars = slice0.variables
        slice1Vars = tuple(v + '_' for v in slice1.variables)
        timeData = slice0.addColumns(slice1Vars, slice1.data)

        # Dict to store data weights for subsets of variables
        emWeights = {}

        for i in range(nNetworks):
            # Select variables for learning (randomization)
            if testAttr is not None:
                trainAttr = random.sample(set(slice0.variables) -
                                          set(testAttr), nAttr - 1)
                attr = trainAttr + list(testAttr)
            else:
                attr = random.sample(set(slice0.variables), nAttr)
            attr = tuple(sorted(attr))

            if attr not in emWeights:
                # Perform EM to assign weights to instances for bagging
                subdata = slice0.subdatabase(attr)
                dataWeights, priors = \
                    self._computeDataWeights(subdata, k, emItr)
                emWeights[attr] = (dataWeights, priors)
            else:
                dataWeights, priors = emWeights[attr]

            # Perform weighted bagging and learn a network
            while True:
                try:
                    weights = util.choice(dataWeights, priors)

                    # Learn a dynamic network
                    attr_ = tuple(a + '_' for a in attr)
                    timeAttr = attr + attr_
                    bag = timeData.bagPSD(timeAttr, bagSize, weights,
                                          maxTries=100)
                    slice0 = bag.subdatabase(attr)
                    slice1 = bag.subdatabase(attr_)
                    slice1._varNames = tuple(attr)  # Gross hack
                    network = learning.learnDBN(slice0, slice1, learn)
                    self._models.append(network)
                    break
                except bayeserror.MaxTriesExceeded as e:
                    # Rerun EM if weighted bagging fails
                    print(e)
                    dataWeights, priors = \
                        self._computeDataWeights(subdata, k, emItr)
                    emWeights[attr] = (dataWeights, priors)

        if testAttr is not None:
            # Compute component weights (improves randomization accuracy)
            trainAttr = tuple(set(slice0.variables) - set(testAttr))
            self._weights = self._computeComponentWeights(timeData, trainAttr,
                                                          testAttr)

    @util.logged
    def _computeComponentWeights(self, data, trainAttr, testAttr):
        '''
        Perform regression to assign a fixed weight to each component of the
        ensemble. This should improve prediction accuracy when using
        randomization (see section 3.1 of Hellman).
        '''

        data, junk = data.randomPartition(len(data) // 10)

        # Test on testAttr at time slice 1 only
        trainAttr = trainAttr + testAttr + tuple(a + '_' for a in trainAttr)
        testAttr = tuple(a + '_' for a in testAttr)
        train = data.subdatabase(trainAttr)
        target = data.subdatabase(testAttr)
        train._varNames = tuple(a + ('1' if a.endswith('_') else '_0')
                                for a in train.variables)
        target._varNames = tuple(a + ('1' if a.endswith('_') else '_0')
                                 for a in target.variables)

        # Compute prediction for each model on each instance
        predictions = np.zeros((len(data), len(self)))
        likelihoods = np.zeros((len(data), len(self)))

        for i, inst in enumerate(train.instances()):
            for j, dist in enumerate(self.toGMM(n=1)):
                reducedInst = {v: val for v, val in inst.items()
                               if v in dist.variables}
                predictions[i, j] = dist.inference(target.variables,
                                                   reducedInst).mean
                likelihoods[i, j] = (dist.partition(reducedInst.keys()).
                                     pdf(reducedInst))

        # Weight predictions by likelihoods
        predictions = ((predictions * likelihoods).T /
                       likelihoods.sum(axis=1)).T

        # NaNs may have been produced in the previous calculation for instances
        # whose likelihood is zero for all ensemble components. The weights for
        # these instances should be zero.
        predictions = np.nan_to_num(predictions)

        # Use ridge regression to compute component weights
        ridge = Ridge(fit_intercept=False)
        ridge.fit(predictions, target.data)
        weights = ridge.coef_.flatten()
        #print('rr weights:\t{}'.format(weights))
        return weights

    def toGMM(self, n=1):
        '''
        Convert the ensemble to a list of Gaussian distributions.
        '''

        if self._gmm is None:
            self._gmm = {}

        if n not in self._gmm:
            gmm = []
            for dbn in self._models:
                network = dbn.unroll(n)
                gmm.append(cstats.Gaussian(network=network,
                                           varNames=network.nodes()))
            self._gmm[n] = gmm

        return self._gmm[n]

    def inference(self, query, inst, n=1):
        prediction = 0
        weightSum = 0

        for dist, weight in zip(self.toGMM(n=n), self._weights):
            nodes = set(dist.variables)
            reducedQuery = set(query) & set(nodes)
            reducedInst = {node: val for node, val in inst.items()
                           if node in nodes}

            # Perform inference using the equivalent multivariate Gaussian
            # distribution
            p = dist.partition(reducedInst.keys()).pdf(reducedInst)
            result = dist.inference(reducedQuery, reducedInst)

            prediction += p * result.mean * weight
            weightSum += p

        # Return 0 if no one can make a decent prediction
        if weightSum == 0:
            return 0

        return prediction / weightSum

    def predictNext(self, query, inst):
        '''
        Predict the set of query variables at the next time step given an
        instantiation of variables at the current time step.
        '''

        query = {v + '_1' for v in query}
        inst = {v + '_0': val for v, val in inst.items()}

        '''
        prediction = self.inference(query, inst, n=1)
        print('{}\t{}'.format(inst['CREFD_MAX_0'], prediction))
        return prediction
        '''
        return self.inference(query, inst, n=1)


class Hellman(LGNEnsemble):
    def __init__(self, data=None, nNetworks=10, nAttr=None, testAttr=None,
                 bagSize=None, alg='MMHC', models=None):

        if models is None:
            models = []

        if nAttr is None:
            nAttr = len(data.variables)

        self._models = list(models)
        self._gmm = None
        self._weights = np.ones(nNetworks)

        if alg == 'FAS':
            learn = learning.FAS
        elif alg == 'HC':
            learn = learning.hillClimbing
        elif alg == 'MMHC':
            learn = learning.MMHC
        else:
            raise ValueError('\'{}\' is not a valid learning algorithm'
                             .format(alg))

        if data is not None:
            for i in range(nNetworks):
                # Perform traditional bagging and randomization
                if testAttr is not None:
                    trainAttr = random.sample(set(data.variables) -
                                              set(testAttr), nAttr - 1)
                    attr = trainAttr + list(testAttr)
                else:
                    attr = random.sample(set(data.variables), nAttr)
                attr = tuple(sorted(attr))

                bag = data.bagPSD(attr, bagSize)
                network = learn(bag)
                learning.estimateParams(network, bag)
                self._models.append(network)

            if testAttr is not None:
                # Compute component weights (improves randomization accuracy)
                trainAttr = set(data.variables) - set(testAttr)
                self._weights = self._computeComponentWeights(data, trainAttr,
                                                              testAttr)
