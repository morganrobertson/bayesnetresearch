import itertools
import random

import numpy as np

import bayeserror
import cstats
import util

# Define constants for types of data
DISCRETE = 0
CONTINUOUS = 1


class TableDB:
    def __init__(self, varNames, data, coords=None, times=None,
                 datatype=CONTINUOUS):
        '''
        Initialize a table database.

        varNames : A sequence of variable names corresponding to each column of
            the dataset.
        data : A dataset where each column corresponds to a particular variable
            and each row is an instance of data.
        coords : An optional sequence of coordinates for the data.
        times : An optional sequence of timestamps for the data.
        datatype : The type of data in this database.
        '''

        self._varNames = tuple(varNames)
        self.data = np.array(data)
        if coords is not None:
            if len(coords) != len(data):
                raise ValueError('The length of coords must be equal to the '
                                 'length of data')
            self._coords = np.array(coords)
        if times is not None:
            if len(times) != len(data):
                raise ValueError('The length of times must be equal to the '
                                 'length of data')
            self._times = np.array(times)
        self._datatype = datatype
        self.__mean = None
        self.__covariance = None

        # Check that variables match up with columns
        m, n = self.data.shape
        if n != len(self._varNames):
            raise ValueError('The number of variables must match the number '
                             'of columns in the data table')

        # Map variables to their respective values for each instance
        self.dataDict = {varName: tuple(vals) for varName, vals in
                         zip(varNames, np.transpose(data))}

        # Map variables to their possible values
        self.valsDict = {varName: set(self.dataDict[varName]) for
                         varName in varNames}

    def __len__(self):
        '''
        Get the number of instances in the dataset.
        '''

        return self.data.shape[0]

    def __getitem__(self, idx):
        # TODO: parse the slice to get correct varNames, times, and coords
        varNames = self._varNames
        data = self.data[idx]
        coords = self._coords if hasattr(self, '_coords') else None
        times = self._times if hasattr(self, '_times') else None
        return TableDB(varNames, data, coords=coords, times=times,
                       datatype=self._datatype)

    def __iter__(self):
        return iter(self.data)

    def instances(self):
        '''
        Generator that yields an instance on each call. The yielded instance
        is a dictionary that maps variable names to values.
        '''

        for inst in self.data:
            yield {v: val for v, val in zip(self._varNames, inst)}

    @property
    def datatype(self):
        return self._datatype

    @property
    def variables(self):
        '''
        Get the names of variables in the table.
        '''

        return self._varNames

    def colIdx(self, varName):
        '''
        Get the column index of a variable in the database.
        '''

        return self._varNames.index(varName)

    def getData(self, varName):
        '''
        Get the data for a particular variable.

        varName : The name of the variable to get data for.
        '''

        idx = self._varNames.index(varName)
        return self.data[:, idx]

    @property
    def coords(self):
        '''
        Get the coordinates for instances in the database.
        '''

        try:
            return self._coords
        except AttributeError:
            raise AttributeError('Database contains no coordinates')

    @property
    def times(self):
        '''
        Get the timestamps for instances in the database.
        '''

        try:
            return self._times
        except AttributeError:
            raise AttributeError('Database contains no timestamps')

    def values(self, varName):
        '''
        Get the set of values that varName takes on in the dataset.

        varName : The name of the variable to get values for.
        '''

        return self.valsDict[varName]

    def toDict(self, varNames=None):
        '''
        Get a dictionary that maps variable names to their respective values
        for each instance in the table.

        varNames : The names of the variables to retrieve data for. If varNames
            is not specified, data for all variables is returned.
        '''

        if varNames is None:
            return dict(self.dataDict)
        else:
            return {varName: self.dataDict[varName] for varName in varNames}

    def valuesDict(self, varNames=None):
        '''
        Get a dictionary that maps variable names to the values that they take
        on in the dataset.

        varNames : The names of the variabes to retrieve data for. If no
            variables are specified, data for all variables is returned.
        '''

        if varNames is None:
            return dict(self.valsDict)
        else:
            return {varName: self.valsDict[varName] for varName in varNames}

    def subtable(self, varNames):
        '''
        Get a table of data corresponding to a subset of variables.

        varNames : The names of the variables to retrieve data for. The order
            of columns in the returned table will correspond to the order of
            these variables.
        '''

        idx = [self._varNames.index(varName) for varName in varNames]
        return self.data[:, idx]

    def subdatabase(self, varNames):
        '''
        Get a database corresponding to a subset of variables.

        varNames : The names of the variables to retrieve data for. The order
            of columns in the returned database will correspond to the order of
            these variables.
        '''

        data = self.subtable(varNames)
        coords = self._coords if hasattr(self, '_coords') else None
        times = self._times if hasattr(self, '_times') else None
        return TableDB(varNames, data, coords=coords, times=times,
                       datatype=self._datatype)

    def size(self):
        '''
        Get the size of the dataset.
        '''

        return self.data.shape

    def cardinality(self, varName):
        '''
        Get the number of different values varName takes on in the dataset.

        varName : The name of the variable to retrieve the cardinality of.
        '''

        if self._datatype == CONTINUOUS:
            raise TypeError('Cardinality is undefined for continuous data')

        return len(self.valsDict[varName])

    def count(self, inst):
        '''
        Count how many instances in the dataset contain a certain instantiation
        of variables.

        inst : A mapping from variable names to values. This can be a partial
            or full instantiation of variables.
        '''

        reducedData = tuple(zip(*[self.dataDict[key] for key in inst.keys()]))
        return reducedData.count(tuple(inst.values()))

    def simpleContingencyTable(self, varA, varB, condInst=None):
        '''
        Get an unlabelled contingency table for two variables. The table
        returned contains only frequency counts. No guarantees are made about
        which cell corresponds to which attibrute value.

        varA, varB : The names of the variables to construct the table for.
        condInst : A mapping from variable names to values that will be used
            for conditioning when constructing the table. If not specified, no
            variables will be used for conditioning.
        '''

        if condInst is None:
            condInst = {}

        return np.array(
            [[self.count(dict({varA:valA, varB:valB}, **condInst))
                for valA in self.values(varA)]
                for valB in self.values(varB)]
        )

    def instantiations(self, varNames):
        '''
        Get all possible instantiations of the specified variables. A tuple
        of dict objects is returned, where each dict is a an instantiation (a
        mapping from variable names to their respective values).

        varNames : The names of the variables to be jointly instantiated.
        '''

        if self._datatype == CONTINUOUS:
            raise TypeError('There are infinitely many instantiations for '
                            'continuous data')

        values = self.valuesDict(varNames)
        product = itertools.product(*values.values())

        return tuple({varName: val for varName, val in
                      zip(values.keys(), vals)} for vals in product)

    def mean(self, varNames=None):
        '''
        Get the mean of each variable in the dataset.

        varNames : The names of the variables to retrieve data for. The order
            of values in the returned array will correspond to the order of
            these variables. If not given, the mean for each variable will be
            returned.
        '''

        if self._datatype != CONTINUOUS:
            raise TypeError('The mean can only be calculated for continuous '
                            'data')

        # Calculate and cache mean vector
        if self.__mean is None:
            self.__mean = np.mean(self.data, axis=0)

        # Obtain indices for variable names
        if varNames is None:
            idx = [i for i in range(len(self._varNames))]
        else:
            idx = [self._varNames.index(varName) for varName in varNames]

        return self.__mean[idx]

    def covariance(self, varNames=None):
        '''
        Calculate the covariance matrix for the dataset.

        varNames : The names of the variables to retrieve data for. The order
            of values in the returned array will correspond to the order of
            these variables. If not given, the covariance for each pair of
            variables will be returned.
        '''

        if self._datatype != CONTINUOUS:
            raise TypeError('The covariance matrix can only be calculated for '
                            'continuous data')

        # Calculate and cache covariance matrix
        if self.__covariance is None:
            self.__covariance = np.cov(self.data, rowvar=0, bias=1)

        # Obtain indices for variable names
        if varNames is None:
            idx = [i for i in range(len(self._varNames))]
        else:
            idx = [self._varNames.index(varName) for varName in varNames]

        return self.__covariance[idx, :][:, idx]

    def bag(self, attr=None, nSamples=None, weights=None):
        '''
        Get a TableDB containing data from this database sampled with
        replacement.

        attr : Either a collection of attributes (variables) to sample for or
            an integer specifying how many attributes to sample for. If an
            integer is given, a subset of attributes is randomly chosen. If not
            given, all attributes are used.
        nSamples : The number of samples to generate. If not specified, the
            number of instances in the returned database is equal to the number
            of instances in this database.
        weights : A sequence of weights where each element corresponds to an
            instances in the database. This allows for weighted sampling. If
            not given, the weights are considered to be uniform.
        '''

        # Select variable indices
        if attr is None:
            attr = self._varNames
        elif type(attr) is int:
            attr = random.sample(self._varNames, attr)
        varIdx = [self._varNames.index(varName) for varName in attr]

        if nSamples is None:
            nSamples = len(self)

        # Select data instance indices
        if weights is None:
            dataIdx = np.random.randint(0, len(self), nSamples)
        else:
            weights = np.array(weights) / np.sum(weights)  # Normalize
            dataIdx = np.random.choice(np.arange(len(self)), size=nSamples,
                                       p=weights)

        baggedData = self.data[dataIdx, :][:, varIdx]
        coords = self._coords[dataIdx] if hasattr(self, '_coords') else None
        times = self._times[dataIdx] if hasattr(self, '_times') else None

        return TableDB(attr, baggedData, coords=coords, times=times,
                       datatype=self._datatype)

    def bagPSD(self, attr=None, nSamples=None, weights=None, maxTries=None):
        '''
        Get a TableDB containing data from this database sampled with
        replacement. The bagged dataset is guaranteed to have a positive-
        semidefinite covariance matrix.

        attr : Either a collection of attributes (variables) to sample for or
            an integer specifying how many attributes to sample for. If an
            integer is given, a subset of attributes is randomly chosen. If not
            given, all attributes are used.
        nSamples : The number of samples to generate. If not specified, the
            number of instances in the returned database is equal to the number
            of instances in this database.
        weights : A sequence of weights where each element corresponds to an
            instances in the database. This allows for weighted sampling. If
            not given, the weights are considered to be uniform.
        maxTries : The maximum number of times to attempt bagging. If the
            maximum number of tries is exceeded, bayeserror.MaxTriesExceeded is
            raised.
        '''

        tries = 0
        while maxTries is None or tries < maxTries:
            bag = self.bag(attr, nSamples, weights)

            if cstats.isPositiveDefinite(bag.covariance()):
                return bag

            # TODO: remove
            print('bagPSD(): Rebagging due to non-PSD covariance!')
            tries += 1

        raise bayeserror.MaxTriesExceeded('A bag with a PSD covariance matrix '
                                          'could not be obtained for the '
                                          'given database')

    def sample(self, attr=None, weights=None):
        '''
        Get a random instance from the database.

        attr : Either a collection of attributes (variables) to sample for or
            an integer specifying how many attributes to sample for. If an
            integer is given, a subset of attributes is randomly chosen. If not
            given, all attributes are used.
        weights : A sequence of weights where each element corresponds to an
            instances in the database. This allows for weighted sampling. If
            not given, the weights are considered to be uniform.
        '''

        # Select variable indices
        if attr is None:
            attr = self._varNames
        elif type(attr) is int:
            attr = random.sample(self._varNames, attr)
        varIdx = [self._varNames.index(varName) for varName in attr]

        if weights is None:
            dataIdx = np.random.randint(0, len(self))
        else:
            weights = np.array(weights) / np.sum(weights)  # Normalize
            dataIdx = np.random.choice(np.arange(len(self)), p=weights)
        return self.data[dataIdx, varIdx]

    def partition(self, size):
        '''
        Partition the database into two databases.

        size : The number of instances in the first database. The second
            database consists of the remaining instances.
        '''

        data1 = self.data[:size]
        coords1 = self._coords[:size] if hasattr(self, '_coords') else None
        times1 = self._times[:size] if hasattr(self, '_times') else None
        db1 = TableDB(self._varNames, data1, coords=coords1, times=times1,
                      datatype=self._datatype)

        data2 = self.data[size:]
        coords2 = self._coords[size:] if hasattr(self, '_coords') else None
        times2 = self._times[size:] if hasattr(self, '_times') else None
        db2 = TableDB(self._varNames, data2, coords=coords2, times=times2,
                      datatype=self._datatype)

        return db1, db2

    def randomPartition(self, size):
        '''
        Randomly partition the database into two databases.

        size : The number of instances in the first database. The second
            database consists of the remaining instances.
        '''

        idx = np.arange(len(self))
        np.random.shuffle(idx)

        idx1 = idx[:size]
        idx2 = idx[size:]

        data1 = self.data[idx1]
        coords1 = self._coords[idx1] if hasattr(self, '_coords') else None
        times1 = self._times[idx1] if hasattr(self, '_times') else None
        db1 = TableDB(self._varNames, data1, coords=coords1, times=times1,
                      datatype=self._datatype)

        data2 = self.data[idx2]
        coords2 = self._coords[idx2] if hasattr(self, '_coords') else None
        times2 = self._times[idx2] if hasattr(self, '_times') else None
        db2 = TableDB(self._varNames, data2, coords=coords2, times=times2,
                      datatype=self._datatype)

        return db1, db2

    def biasSample(self, condition, proportion=0.5, nSamples=None):
        '''
        Perform under- or oversampling, which can be used to reduce class bias
            in a dataset.

        condition : A callable that distinguishes between two groups (or
            classes) by returning a boolean values given an instances of data.
            An instance that evaluates to True is considered a positive
            instance while an instance that evaluates to False is considered a
            negative instance.
        proportion : The desired proportion of positive instances in the
            returned database.
        nSamples : The number of samples to generate. If not specified, the
            number of instances in the returned database is equal to the number
            of instances in this database.
        '''

        classes = condition(self.data)
        nPositive = np.count_nonzero(classes)
        nNegative = len(self) - nPositive
        posWeight = proportion / nPositive
        negWeight = (1 - proportion) / nNegative
        weights = np.array([posWeight if class_ else negWeight
                            for class_ in classes])

        return self.bag(nSamples=nSamples, weights=weights)

    def biasSamplePSD(self, condition, proportion=0.5, nSamples=None):
        '''
        Perform under- or oversampling, which can be used to reduce class bias
            in a dataset. The sampled dataset is guaranteed to have a positive-
            semidefinite covariance matrix.

        condition : A callable that distinguishes between two groups (or
            classes) by returning a boolean values given an instances of data.
            An instance that evaluates to True is considered a positive
            instance while an instance that evaluates to False is considered a
            negative instance.
        proportion : The desired proportion of positive instances in the
            returned database.
        nSamples : The number of samples to generate. If not specified, the
            number of instances in the returned database is equal to the number
            of instances in this database.
        '''

        classes = condition(self.data)
        nPositive = np.count_nonzero(classes)
        nNegative = len(self) - nPositive
        posWeight = proportion / nPositive
        negWeight = (1 - proportion) / nNegative
        weights = np.array([posWeight if class_ else negWeight
                            for class_ in classes])

        return self.bagPSD(nSamples=nSamples, weights=weights)

    def transform(self, func, varNames=None):
        '''
        Perform a transformation on data in the database. A new TableDB object
        is returned.

        func : A callable to perform the transformation. The callable will be
            called repeatedly with data for each variable to be transformed.
            The callable must return a sequence the same length as the array
            passed to it.
        varNames : The variables to perform the transformation on. If None, all
            variables are transformed.
        '''

        # Obtain indices for variable names
        if varNames is None:
            varNames = self._varNames
            idx = [i for i in range(len(self._varNames))]
        else:
            idx = [self._varNames.index(varName) for varName in varNames]

        data = np.array(self.data)
        for i in idx:
            data[:, i] = func(self.data[:, i])

        coords = self._coords if hasattr(self, '_coords') else None
        times = self._times if hasattr(self, '_times') else None
        return TableDB(self._varNames, data, coords=coords, times=times,
                       datatype=self._datatype)

    def addColumns(self, varNames, data):
        '''
        Add data for new variables to the database. A new database with the
        added columns is returned.

        varNames : A sequence of variable names corresponding to each column of
            the data.
        data : An array containing the columns to be added to the database.
        '''

        varNames = self._varNames + varNames
        data = np.column_stack((self.data, data))
        coords = self._coords if hasattr(self, '_coords') else None
        times = self._times if hasattr(self, '_times') else None
        return TableDB(varNames, data, coords=coords, times=times,
                       datatype=self._datatype)

    def saveCSV(self, filename):
        '''
        Save the database as a comma separated value (CSV) file.

        filename : The name of the CSV file to write to.
        '''

        import csv

        with open(filename + '.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(self._varNames)
            writer.writerows(self.data)


def TableDBFromNetCDF(filename, varNames, coordsDims=None, timeDim=None,
                      verticalMax=False, **dimensions):
    '''
    Make a database from a NetCDF file.

    varNames : A collection of variable names to retrieve data for. If None,
        data for all variables is retrieved.
    filename : The name of the NetCDF file to extract data from.
    coordsDims : A sequence of dimension names to use as coordinates in the
        created database.
    timeDim : The name of the dimension to use for timestamps in the created
        database.
    dimensions : A set of key=value mappings where each key is a dimension in
        the dataset and the value is an index or list of indices that
        correspond to the indices to retrieve data for.
    '''

    import netCDF4
    import time

    with netCDF4.Dataset(filename) as dataset:
        if varNames is None:
            varNames = [v for v in dataset.variables]

        variables = [dataset.variables[v] for v in varNames]

        dataList = []
        for v in variables:
            idx = tuple(dimensions[d] for d in v.dimensions)
            start = time.time()
            data = v[idx]
            print('get data time\t{}'.format(round(time.time() - start, 4)))

            # Use verticalMax=True with caution.. makes brittle assumptions
            if verticalMax and 'bottom_top' in v.dimensions:
                data = np.max(data, axis=0)  # Gross hack: axis should be
                                             # computed properly

            dataList.append(data)

        data = np.array(dataList).T
        coords = (np.array([dimensions[d] for d in coordsDims]).T if
                  coordsDims is not None else None)
        times = np.array(dimensions[timeDim]) if timeDim is not None else None

        return TableDB(varNames, data, coords=coords, times=times)


def TableDBFromCSV(filename, varNames=None, coordsDims=None, timeDim=None,
                   delimiter=',', strip=None, convert=np.double):
    '''
    Make a database from a CSV file.

    varNames : A collection of variable names to retrieve data for. If None,
        data for all variables is retrieved.
    filename : The name of the CSV file to extract data from.
    coordsDims : A sequence of variable names to use as coordinates in the
        created database.
    timeDim : The name of the variable to use for timestamps in the created
        database.
    delimiter : The delimiter to use for parsing variable names from the file.
    strip : The characters to strip from the ends of the parsed variable names.
        If None, whitespace is stripped.
    convert : A function to apply to each element. Elements are converted to
        np.double by default.
    '''

    import csv

    with open(filename, newline='') as f:
        # The first line should contain a list of variable names
        datasetVars = [v.strip(strip) for v in next(f).split(delimiter)]

        data = []
        reader = csv.reader(f, delimiter=delimiter)
        for row in reader:
            data.append([convert(entry) for entry in row])

    coords = None
    times = None
    allData = np.array(data)

    if varNames is not None:
        idx = [datasetVars.index(varName) for varName in varNames]
        data = allData[:, idx]
    else:
        varNames = datasetVars

    if coordsDims is not None:
        idx = [datasetVars.index(dim) for dim in coordsDims]
        coords = allData[:, idx]

    if timeDim is not None:
        idx = datasetVars.index(timeDim)
        times = allData[:, idx]

    return TableDB(varNames, data, coords=coords, times=times)
