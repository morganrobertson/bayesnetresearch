class BayesException(Exception):
    '''
    Base class for exceptions.
    '''


class MaxTriesExceeded(BayesException):
    '''
    Raised when the maximum number of tries have been exceeded for some
    operation.
    '''
