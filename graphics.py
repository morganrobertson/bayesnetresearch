import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np

from mpl_toolkits.mplot3d import Axes3D

import cstats
import learning
import util


def plotSurface2D(x, y, z, xRes=100, yRes=100, nLevels=10, interp='nn',
                  fig=None):
    if fig is None:
        fig = plt.figure()
    ax = fig.gca()

    xMin, xMax = x.min(), x.max()
    yMin, yMax = y.min(), y.max()

    xi = np.linspace(xMin, xMax, xRes)
    yi = np.linspace(yMin, yMax, yRes)
    zi = mlab.griddata(x, y, z, xi, yi, interp=interp)

    cs = ax.contourf(xi, yi, zi, nLevels, cmap=plt.cm.rainbow,
                     vmax=zi.max(), vmin=zi.min())
    ax.scatter(x, y, s=1)
    plt.colorbar(cs)

    return fig


def plotSurface3D(x, y, z, xRes=100, yRes=100, interp='nn', plottype='surface',
                  fig=None):
    if fig is None:
        fig = plt.figure()
    ax = fig.gca(projection='3d')

    xMin, xMax = x.min(), x.max()
    yMin, yMax = y.min(), y.max()

    xi, yi = np.mgrid[xMin:xMax:xRes * 1j, yMin:yMax:yRes * 1j]
    zi = mlab.griddata(x, y, z, xi, yi, interp=interp)

    if plottype == 'surface':
        ax.plot_surface(xi, yi, zi, cmap=plt.cm.rainbow, vmax=zi.max(),
                        vmin=zi.min())
    elif plottype == 'wireframe':
        ax.plot_wireframe(xi, yi, zi, rstride=10, cstride=10)

    return fig


def plotLinearRegression2D(data, x, y, xRange=None, yRange=None, color='k',
                           linestyle='-', plotData=True, mark='x', fig=None):
    if fig is None:
        fig = plt.figure()
    ax = fig.gca()

    if plotData:
        ax.scatter(data.getData(x), data.getData(y), marker=mark)

    if xRange is None:
        xRange = ax.get_xlim()
    else:
        ax.set_xlim(*xRange)
    if yRange is None:
        yRange = ax.get_ylim()
    else:
        ax.set_ylim(*yRange)

    beta = learning.linearRegression(data, (x,), (y,))

    yLeft = beta.dot([1, xRange[0]])
    xLeft = xRange[0]
    if yLeft < yRange[0]:
        yLeft = yRange[0]
        xLeft = (yLeft - beta[0]) / beta[1]
    elif yLeft > yRange[1]:
        yLeft = yRange[1]
        xLeft = (yLeft - beta[0]) / beta[1]

    yRight = beta.dot([1, xRange[1]])
    xRight = xRange[1]
    if yRight < yRange[0]:
        yRight = yRange[0]
        xRight = (yRight - beta[0]) / beta[1]
    elif yRight > yRange[1]:
        yRight = yRange[1]
        xRight = (yRight - beta[0]) / beta[1]

    ax.plot([xLeft, xRight], [yLeft, yRight], color=color, linestyle=linestyle)

    return fig


def plotBivariateNormal(mean, cov, xRange=None, yRange=None, xDelta=0.1,
                        yDelta=0.1, mark=None, fig=None):
    '''
    Plot a bivariate normal distribution.

    mean : The mean vector of the distribution.
    cov : The covariance matrix of the distribution.
    xRange : The range of x values to plot.
    yRange : The range of y values to plot.
    xDelta : The plotting resolution for the x-axis.
    yDelta : The plotting resolution for the y-axis.
    mark : The mark to use to plot the mean.
    fig : A figure to be drawn on.
    '''

    if fig is None:
        fig = plt.figure()

    ax = fig.gca()
    if xRange is None:
        xRange = ax.get_xlim()
    if yRange is None:
        yRange = ax.get_xlim()

    x = np.arange(*xRange, step=xDelta)
    y = np.arange(*yRange, step=yDelta)
    X, Y = np.meshgrid(x, y)
    Z = mlab.bivariate_normal(X, Y, cov[0, 0], cov[1, 1], mean[0], mean[1],
                              cov[0, 1])

    if mark is not None:
        plt.plot(mean[0], mean[1], mark)

    return plt.contour(X, Y, Z, 1)


def plotCovarianceLine(mean, cov, xRange=None, yRange=None, color='k',
                       linestyle='-', fig=None):
    '''
    Plot the covariance line for a bivariate normal distribution. The
    covariance line passes through the mean and has a slope equal to the
    covariance of the two variables.

    mean : The mean vector of the distribution.
    cov : The covariance matrix of the distribution.
    xRange : The range of x values to plot.
    yRange : The range of y values to plot.
    color : The color of the plotted line.
    linestyle : The style of the plotted line.
    fig : A figure to be drawn on.
    '''

    if fig is None:
        fig = plt.figure()

    ax = fig.gca()
    if xRange is None:
        xRange = ax.get_xlim()
    if yRange is None:
        yRange = ax.get_ylim()

    slope = cov[0, 1]
    intercept = mean[1] - slope * mean[0]

    yLeft = slope * xRange[0] + intercept
    xLeft = xRange[0]
    if yLeft < yRange[0]:
        yLeft = yRange[0]
        xLeft = (yLeft - intercept) / slope
    elif yLeft > yRange[1]:
        yLeft = yRange[1]
        xLeft = (yLeft - intercept) / slope

    yRight = slope * xRange[1] + intercept
    xRight = xRange[1]
    if yRight < yRange[0]:
        yRight = yRange[0]
        xRight = (yRight - intercept) / slope
    elif yRight > yRange[1]:
        yRight = yRange[1]
        xRight = (yRight - intercept) / slope

    ax.plot(mean[0], mean[1], 'ro')
    ax.plot([xLeft, xRight], [yLeft, yRight], color=color, linestyle=linestyle)

    return fig


def plotNormalRegression(mean, cov, xRange=None, yRange=None, color='k',
                         linestyle='-', fig=None):
    '''
    Plot the linear regression line for a bivariate normal distribution.

    mean : The mean vector of the distribution.
    cov : The covariance matrix of the distribution.
    xRange : The range of x values to plot.
    yRange : The range of y values to plot.
    color : The color of the plotted line.
    linestyle : The style of the plotted line.
    fig : A figure to be drawn on.
    '''

    if fig is None:
        fig = plt.figure()

    ax = fig.gca()
    if xRange is None:
        xRange = ax.get_xlim()
    if yRange is None:
        yRange = ax.get_ylim()

    distribution = cstats.Gaussian(mean, cov, ('x', 'y'))

    print('mean:\t{}'.format(mean))

    yLeft = distribution.inference({'y'}, {'x': xRange[0]}).mean[0]
    xLeft = xRange[0]
    if yLeft < yRange[0]:
        print('\tyLeft too small')
        yLeft = yRange[0]
        xLeft = distribution.inference({'x'}, {'y': yLeft}).mean[0]
    elif yLeft > yRange[1]:
        print('\tyLeft too big')
        yLeft = yRange[1]
        xLeft = distribution.inference({'x'}, {'y': yLeft}).mean[0]

    yRight = distribution.inference({'y'}, {'x': xRange[1]}).mean[0]
    xRight = xRange[1]
    if yRight < yRange[0]:
        print('\tyRight too small')
        yRight = yRange[0]
        xRight = distribution.inference({'x'}, {'y': yRight}).mean[0]
    elif yRight > yRange[1]:
        print('\tyRight too big')
        yRight = yRange[1]
        xRight = distribution.inference({'x'}, {'y': yRight}).mean[0]

    ax.plot(mean[0], mean[1], 'ro')
    ax.plot([xLeft, xRight], [yLeft, yRight], color=color, linestyle=linestyle)
    print(xLeft, xRight, yLeft, yRight)

    return fig


@util.logged
def plotGMR(distributions, varNames, xRange=None, yRange=None, delta=None,
            color='k', linestyle='-', meanMark=None, fig=None):

    if fig is None:
        fig = plt.figure()

    ax = fig.gca()
    if xRange is None:
        xRange = ax.get_xlim()
    if yRange is None:
        yRange = ax.get_ylim()
    if delta is None:
        delta = (xRange[1] - xRange[0]) / 100

    fixedVar, predicted = varNames
    vals = np.arange(xRange[0], xRange[1], delta)

    # Create an array to hold values of the dependent variable
    predictions = np.zeros(vals.size)

    # Calculate predictions for the dependent variable
    for i, val in enumerate(vals):
        prediction = 0.0
        weightSum = 0.0

        for dist in distributions:
            weight = dist.partition({fixedVar}).pdf({fixedVar: val})
            prediction += weight * dist.inference({predicted},
                                                  {fixedVar: val}).mean[0]
            weightSum += weight

        predictions[i] = prediction / weightSum

    ax.plot(vals, predictions, color=color, linestyle=linestyle)

    # Plot means of model components
    if meanMark is not None:
        for dist in distributions:
            mean = dist.partition(varNames).mean
            ax.scatter(mean[0], mean[1], color=color, marker=meanMark)

    return fig


def scatterPlotMatrix(data, plottype='scatter', mark='x', fig=None, func=None,
                      plotAll=False):
    '''
    Create a scatter plot matrix for a dataset. In each subplot, a pair of
    variables is plotted against each other.

    data : The dataset to plot.
    plottype : A string specifying the type of plot to use. Valid arguments are
        'scatter' for a normal scatter plot and 'hexbin' for a hexbin plot.
    mark : The mark to use for normal scatter plots.
    fig : A figure to be drawn on.
    func : A function to call after each subplot is drawn. The function is
        passed the data, a 2-tuple containing the independent and dependent
        variables, and the figure being drawn on.
    plotAll : Specifies whether or not to plot the entire matrix. If False,
        only the lower left portion of the matrix is plotted.
    '''

    if fig is None:
        fig = plt.figure()
    nVars = len(data.variables)

    def onEnter(event):
        ax = event.inaxes
        for text in ax.texts:
            text.set_visible(True)
        plt.draw()

    def onExit(event):
        ax = event.inaxes
        for text in ax.texts:
            text.set_visible(False)
        plt.draw()

    for i, var1 in enumerate(data.variables):
        for j, var2 in enumerate(data.variables):
            if j <= i or plotAll:
                ax = fig.add_subplot(nVars, nVars, i * nVars + j + 1)

                if plottype == 'scatter':
                    ax.scatter(data.getData(var1), data.getData(var2),
                               marker=mark)
                elif plottype == 'hexbin':
                    ax.hexbin(data.getData(var1), data.getData(var2),
                              bins='log')
                else:
                    raise ValueError('Unrecognized plot type.')

                cov = data.covariance((var1, var2))[0, 1]

                annotation = '{} vs {}\n'.format(var2, var1)
                annotation += 'cov: {:0.3f}'.format(cov)
                an = ax.annotate(annotation, xy=(1, 1), xycoords='axes pixels')
                an.set_visible(False)

                ax.set_xticks([])
                ax.set_yticks([])

                if func is not None:
                    func(data, (var1, var2), fig)

    fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0,
                        hspace=0)

    # Display text when mousing over each cell
    fig.canvas.mpl_connect('axes_enter_event', onEnter)
    fig.canvas.mpl_connect('axes_leave_event', onExit)

    return fig


def scatterPlotMatrixGMR(data, distributions, plottype='scatter', dataMark='x',
                         meanMark=None, fig=None, plotAll=False):
    def gmr(data, varNames, fig):
        if varNames[0] != varNames[1]:
            plotGMR(distributions, varNames, color='r', meanMark=meanMark,
                    fig=fig)

    return scatterPlotMatrix(data, plottype=plottype, mark=dataMark, fig=fig,
                             func=gmr, plotAll=plotAll)


def covPlot(data, fig=None):
    '''
    Create a 2D visual representation of a covariance matrix.

    data : The data whose covariance matrix is to be visualized.
    fig : A figure to be drawn on.
    '''

    if fig is None:
        fig = plt.figure()

    ax = fig.add_subplot(1, 1, 1)
    ax.matshow(data.covariance(), cmap=plt.cm.gray_r)
    ax.set_xticks([])
    ax.set_yticks([])

    return fig
