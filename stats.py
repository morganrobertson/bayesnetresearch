import itertools
import math

import numpy as np
import scipy.stats

import cstats
import database
import util


def gSquared(data, x, y, condSet=None):
    '''
    Calculate the g-squared statistic.
    Adapted from (Neapolitan 2003).

    data : A TableDB object containing the dataset.
    x, y : The variables to calculate the statistic for.
    condSet : A set of variables to condition on.
    '''

    # TODO: reduce dof for 0 counts? (p602, Neapolitan)

    if condSet is None:
        condSet = set()

    g2 = 0
    dof = 0

    if len(condSet) == 0:
        m = len(data)

        for xVal in data.values(x):
            for yVal in data.values(y):
                s_xy = data.count({x: xVal, y: yVal})
                s_x = data.count({x: xVal})
                s_y = data.count({y: yVal})

                # If s_xy == 0, then we have g2 += 0 * math.log(0)
                if s_xy > 0:
                    g2 += s_xy * math.log((s_xy * m) / (s_x * s_y))
        g2 *= 2
        dof = (data.cardinality(x) - 1) * (data.cardinality(y) - 1)
    else:
        for xVal in data.values(x):
            for yVal in data.values(y):
                for condInst in data.instantiations(condSet):
                    s_xyz = data.count(dict({x: xVal, y: yVal}, **condInst))
                    s_xz = data.count(dict({x: xVal}, **condInst))
                    s_yz = data.count(dict({y: yVal}, **condInst))
                    s_z = data.count(condInst)

                    # If s_xyz == 0, then we have g2 += 0 * math.log(0)
                    if s_xyz > 0:
                        g2 += s_xyz * math.log((s_xyz * s_z) / (s_xz * s_yz))
        g2 *= 2

        dof = (data.cardinality(x) - 1) * (data.cardinality(y) - 1)
        for z in condSet:
            dof *= data.cardinality(z)

    return g2, dof


def minAssociation(data, x, y, condSet=None):
    if condSet is None:
        condSet = set()

    minAssoc = None

    if len(condSet) == 0:
        minAssoc = association(data, x, y)
    else:
        for subset in util.subsets(condSet):
            assoc = association(data, x, y, subset)

            if minAssoc is None or assoc < minAssoc:
                minAssoc = assoc

    return minAssoc


def association(data, x, y, condSet=None):
    if condSet is None:
        condSet = set()

    ind, pValue = independentPval(data, x, y, condSet)
    #print('ind:\t{}'.format(ind), end='\t')
    #print('pValue:\t{}'.format(pValue))

    if ind:
        return 0
    else:
        return 1 - pValue


def independent(data, x, y, condSet=None, alpha=0.05, default=True, test=None):
    '''
    Test whether two discrete variables are conditionally independent given
    some conditioning set of variables. The null hypothesis assumes that the
    two variables are independent.

    data : A TableDB object containing the dataset.
    x, y : The variables to test for independence.
    condSet : A set of variables to condition on.
    alpha : The significance threshold at which to reject the null hypothesis.
    default : The default value to return if the test fails (i.e. there are too
        few instances to perform a reliable test).
    test : A string specifying the test to use.
    '''

    if condSet is None:
        condSet = set()

    ind, pValue = independentPval(data, x, y, condSet, alpha, default, test)
    return ind


def independentPval(data, x, y, condSet=None, alpha=0.05, default=True,
                    test=None):
    '''
    Test whether two discrete variables are conditionally independent given
    some conditioning set of variables. The null hypothesis assumes that the
    two variables are independent.

    data : A TableDB object containing the dataset.
    x, y : The variables to test for independence.
    condSet : A set of variables to condition on.
    alpha : The significance threshold at which to reject the null hypothesis.
    default : The default value to return if the test fails (i.e. there are too
        few instances to perform a reliable test).
    test : A string specifying the test to use. If unspecified, a test will be
        chosen based on the type of data.
    '''

    if condSet is None:
        condSet = set()

    if test is None:
        if data.datatype == database.DISCRETE:
            test = 'g-square'
        elif data.datatype == database.CONTINUOUS:
            test = 'pearson'
        else:
            raise ValueError('Data must be either discrete or continuous')

    if test == 'chi-square':
        # TODO: account for small values

        chi2Sum = 0
        dofSum = 0

        if len(condSet) == 0:
            chi2Sum, p, dofSum, ex = scipy.stats.chi2_contingency(
                data.simpleContingencyTable(x, y), False)
        else:
            # Calculate a chi-square statistic for each possible instantiation
            # of the conditioning set of variables
            for condInst in data.instantiations(condSet):
                chi2, p, dof, ex = scipy.stats.chi2_contingency(
                    data.simpleContingencyTable(x, y, condInst), False)

                chi2Sum += chi2
                dofSum += dof

        pValue = scipy.stats.chisqprob(chi2Sum, dofSum)

    elif test == 'g-square':
        g2, dof = gSquared(data, x, y, condSet)
        pValue = scipy.stats.chisqprob(g2, dof)

    elif test == 'pearson':
        corr = cstats.partialCorrelation(data, x, y, condSet)
        dof = math.sqrt(len(data) - len(condSet) - 3)
        z = dof * cstats.fisherZ(corr)
        pValue = 2 * (scipy.stats.zprob(z) if z < 0
                      else (1 - scipy.stats.zprob(z)))

    else:
        raise ValueError('Unknown independence test type: {}'.format(test))

    if pValue < alpha:
        # Reject the null (conclude dependence)
        return False, pValue
    else:
        # Accept the null (conclude independence)
        return True, pValue
