import itertools

import numpy as np

import factor
import util


class CPD():
    '''
    Base class for conditional probability distributions.
    '''

    def __init__(self, child, parents):
        self._child = child
        self._parents = parents

    @property
    def child(self):
        '''
        Get the child variable for this CPD.
        '''

        return self._child

    @property
    def parents(self):
        '''
        Get the set of parent variables for this CPD.
        '''

        return self._parents

    def sample(self, **values):
        '''
        Sample from this CPD given values of parents.
        '''

        pass

    def prob(self, **values):
        '''
        Get the conditional probability that the variable corresponding to this
        CPD is equal to some value.
        '''

        pass


class TableCPD(CPD):

    def __init__(self, child, parents, probs, **var):
        '''
        Initialize a table CPD for a discrete conditional distribution.

        child : The variable for which conditional probabilities are specified
            in this table.
        parents : An iterable containing the variables to condition on.
        probs : A list of probabilities corresponding to each possible
            combination of values of the parent variables.
        '''

        # The table CPD is contained in self.cpd, which is a dict that maps
        # instantiations to probabilities. Each key must be a frozenset of
        # 2-tuples where each 2-tuple contains the variable name and the
        # variable's value for that particular instantiation.

        if type(probs) == dict:
            self.cpd = {inst: p for inst, p in probs.items()}

            self.factor = factor.Factor()
            self.factor.table = self.cpd

        elif type(probs) == factor.Factor:
            self.cpd = probs.table
            self.factor = probs

        else:
            raise ValueError('CPD must be initialized with a Factor type')

        self._child = child
        self._parents = set(parents)
        self.nParams = None  # Calculated and cached when countParameters() is
                             # called

    @property
    def variables(self):
        '''
        Get the set of parent and child variables for this CPD.
        '''

        return self._parents | {self._child}

    def prob(self, inst):
        '''
        Look up a probability from the table given an instantiation of
        variables.

        inst : An instantiation of variables (i.e. a mapping from variable
            names to values).
        '''

        return self.cpd[frozenset(inst.items())]

    def setProb(self, inst, p):
        '''
        Set a conditional probability for an instantiation of variables.

        inst : An instantiation of variables (i.e. a mapping from variable
            names to values).
        p : The conditional probability of the instantiation.
        '''

        self.cpd[frozenset(inst.items())] = p

    def countParameters(self):
        if self.nParams is not None:
            return self.nParams
        else:
            paramCounts = [len(self.getValues(var)) for var in self._parents]
            paramCounts.append(len(self.getValues(self._child)) - 1)
            self.nParams = np.product(paramCounts)
            return self.nParams

    def getValues(self, varName):
        '''
        Get the set of possible values that a variable can take on.

        varName : The name of the variable to get values for.
        '''

        vals = set([pair[1] for s in self.cpd.keys() for pair in s
                   if pair[0] == varName])
        return vals

    def getFactor(self):
        return self.factor

    def __str__(self):
        s = ''
        for instantiation, p in self.cpd.items():
            s += '{}\t|\t{}@@@@\n'.format(instantiation, p)

        return s


class LinearGaussianCPD(CPD):
    '''
    Initialize a CPD for a linear Gaussian distribution.

    #TODO
    child : The variable represented by this conditional probability
        distribution.
    parents : An sequence containing the variables to condition on.
    weights : A sequence of weights. The order of weights must correspond to
        the order of the parents passed to this function.
    #TODO covariance : A 2D NumPy array representing the covariance
    '''

    def __init__(self, child, parents, weight0, weights, variance):
        self._child = child
        self._parents = tuple(parents)
        self._weight0 = weight0
        self._weights = np.array(weights, dtype=np.double)
        self._var = variance
        #TODO self.covariance = np.matrix(covariance)

    @property
    def variables(self):
        '''
        Get the set of parent and child variables for this CPD.
        '''

        return self._parents + (self._child,)

    @property
    def weight0(self):
        '''
        Get the bias weight. This is equal to the unconditional mean of the
        child variable minus the weighted sum of the means of the parents.

        References
        ----------
        (Koller and Friedman, 2009): Theorem 7.3
        '''

        return self._weight0

    @property
    def weights(self):
        '''
        Get the weights corresponding to the parents.
        '''

        return self._weights

    @property
    def variance(self):
        '''
        Get the conditional variance of the child variable given the parents.
        '''

        return self._var

    # TODO: remove setters?
    def setWeight0(self, weight0):
        '''
        Set the offset weight.

        weight0 : The offset weight.
        '''

        self._weight0 = weight0

    def setVariance(self, variance):
        '''
        Set the variance of the child variable.

        variance : The variance of the child variable.
        '''

        self._var = variance

    def setWeight(self, varName, weight):
        '''
        Set the weight for a particular parent variable.

        varName : The variable to set the weight for.
        weight : The weight to be set.
        '''

        self._weights[varName] = weight

    def updateWeights(self, weights):
        '''
        Update the weights.

        weights : A mapping from parent variables to their corresponding
            weights.
        '''

        self._weights = np.array(weights, dtype=np.double)

    def conditionalMean(self, inst):
        '''
        Calculate the mean of the distribution given values of the parent
            variables.

        inst : An instantiation of parent variables (i.e. a mapping from
            variable names to values).
        inst : An instantiation of parent variables. This can be either a
            mapping from variable names to values or an array where each
            element corresponds to a parent variable.
        '''

        '''
        #TODO
        result = self._weight0
        for parent, val in inst.items():
            result += weights[parent] * val
        '''

        if isinstance(inst, dict):
            result = self._weight0
            for parent, w in zip(self._parents, self._weights):
                result += w * inst[parent]
        else:
            result = self._weight0
            result += np.dot(self._weights, inst)

        return result

    def conditionalVariance(self, inst):
        '''
        Calculate the variance of the distribution given values of the parent
            variables.

        inst : An instantiation of parent variables (i.e. a mapping from
            variable names to values).
        '''

        #TODO
        result = self._var
        for parent, val in inst.items():
            result += weights[parent] * val

        return result


class LinearGaussianSpatialCPD(LinearGaussianCPD):
    def __init__(self, child, parents, weight0, weights, variance, scale,
                 kernel=None):
        super().__init__(child, parents, weight0, weights, variance)
        self._scale = scale

        if kernel is None:
            pass
        else:
            self._kernel = kernel

    def conditionalMean(self, inst, coords):
        '''
        Calculate the mean of the distribution given values of the parent
            variables.

        inst : An instantiation of parent variables (i.e. a mapping from
            variable names to values).
        inst : An instantiation of parent variables. This can be either a
            mapping from variable names to values or an array where each
            element corresponds to a parent variable.
        '''

        if isinstance(inst, dict):
            result = self._weight0
            for parent, w in zip(self._parents, self._weights):
                result += w * inst[parent]
        else:
            result = self._weight0
            result += np.dot(self._weights, inst)

        return result


def makeCPD(child, parents, data):
    '''
    Build a CPD for a child variable given its parents using maximum likelihood
    estimation.

    child : The name of the child variable.
    parents : An iterable of parent variables.
    data : A TableDB containing observation data.
    '''

    if data.datatype == database.DISCRETE:
        # Map variables to their respective values for each instance
        dataDict = data.toDict()

        # Map variables to their possible values
        varVals = data.valuesDict()

        varSet = parents + [child]

        # Generate all possible instantiations of the parent variables
        parentInst = itertools.product(*[[(var, val) for val in varVals[var]]
                                         for var in parents])

        probs = {}
        for instantiation in parentInst:
            smalldata = list(zip(*[[(var, val) for val in dataDict[var]]
                                   for var in varSet]))

            # TODO: database can count things
            counts = {}
            totalCount = 0
            for val in varVals[child]:
                fullInst = instantiation + ((child, val),)
                count = smalldata.count(fullInst)
                counts[fullInst] = count
                totalCount += count

            #TODO: should totalCount ever be 0?
            probs.update({frozenset(inst): count / totalCount
                          for inst, count in counts.items() if totalCount > 0})

        return TableCPD(child, parents, probs)
    
    else:
    #TODO
        return CPD.LinearGaussianCPD
