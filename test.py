import time
import numpy as np
import scipy.stats.mstats as mstats
import matplotlib.pyplot as plt
import bayesnet as bn
import bayesio as io
import cpd
import learning
import database
import stats

vals = {'a': {1, 2, 3}, 'b': {4, 5}, 'c': {6, 7, 8}, 'xyz': {9, 0}}
varNames = ('a', 'b', 'c', 'xyz')
data = np.array([
    [3, 5, 8, 0],
    [1, 5, 7, 9],
    [3, 4, 6, 9],
    [1, 4, 7, 0],
    [1, 5, 7, 9],
    [3, 4, 7, 9],
    [1, 4, 7, 9],
    [2, 5, 6, 9],
    [3, 4, 6, 9],
    [1, 5, 7, 0],
    [1, 5, 6, 9],
    [2, 4, 8, 0],
    [1, 5, 7, 0],
    [1, 4, 7, 0],
    [2, 5, 7, 0],
    [3, 4, 8, 9],
    [3, 5, 6, 0],
    [2, 4, 7, 9],
    [3, 5, 8, 9],
    [3, 4, 6, 9],
    [1, 4, 7, 0],
    [2, 4, 8, 9],
    [2, 4, 6, 0],
    [1, 5, 8, 9],
    [1, 5, 7, 9],
    [2, 4, 8, 0],
    [1, 4, 6, 0],
    [1, 5, 7, 9],
    [3, 4, 7, 0],
    [2, 4, 7, 9],
    [2, 4, 6, 0],
    [1, 4, 8, 9],
    [3, 4, 8, 0],
    [3, 4, 8, 0],
    [2, 5, 6, 9],
    [2, 4, 6, 0],
    [1, 4, 7, 9],
    [1, 4, 7, 0],
    [1, 4, 6, 9],
    [1, 4, 8, 0],
    [1, 5, 6, 9],
    [2, 5, 8, 0],
    [1, 4, 7, 0],
    [3, 4, 8, 0],
    [1, 4, 7, 0],
    [1, 4, 7, 9],
    [1, 4, 6, 9],
    [1, 5, 8, 9],
    [2, 4, 6, 9],
    [1, 4, 6, 0],
    [2, 4, 6, 9],
    [2, 4, 6, 9],
    [1, 4, 7, 0],
    [2, 5, 8, 9],
    [1, 5, 8, 0],
    [1, 4, 8, 9],
    [3, 4, 6, 9],
    [1, 5, 8, 0],
    [1, 4, 6, 9],
    [1, 5, 8, 9],
    [3, 5, 8, 0],
    [2, 5, 7, 0],
    [3, 5, 7, 9],
    [2, 5, 8, 9],
    [3, 4, 8, 9],
    [1, 4, 7, 0],
    [3, 5, 7, 9],
    [2, 5, 7, 9],
    [1, 5, 6, 9],
    [1, 5, 6, 9],
    [2, 4, 7, 9],
    [2, 5, 8, 0],
    [1, 4, 6, 9],
    [3, 5, 8, 0],
    [1, 5, 6, 0],
    [2, 5, 7, 9],
    [1, 5, 8, 9],
    [1, 4, 7, 0],
    [1, 5, 6, 9],
    [3, 4, 8, 9],
    [3, 4, 7, 0],
    [1, 4, 8, 9],
    [2, 4, 7, 9],
    [3, 4, 8, 0],
    [1, 5, 6, 9],
    [3, 4, 8, 0],
    [3, 5, 6, 9],
    [1, 5, 7, 0],
    [2, 5, 6, 0],
    [1, 4, 8, 0],
    [3, 4, 8, 0],
    [1, 5, 6, 0],
    [3, 4, 7, 0],
    [1, 4, 6, 0],
    [2, 4, 7, 0],
    [3, 5, 7, 9],
    [1, 5, 7, 0],
    [2, 4, 7, 9],
    [2, 5, 7, 9],
    [2, 4, 8, 0]])
db = database.TableDB(varNames, data)


def testCar():
    data = []
    with open('data/car.data') as f:
        for line in f:
            data.append(line.split(','))

    varNames = ['buying', 'maint', 'doors', 'persons', 'lub_boot', 'safety', 'class']
    table = database.TableDB(varNames, data)

    network1 = Learning.hillClimbing(table)
    network2 = Learning.MMHC(table)
    bic1 = Learning.calculateBIC(network1, data)
    bic2 = Learning.calculateBIC(network2, data)
    print('bic1 = {}'.format(bic1))
    print('bic2 = {}'.format(bic2))

    return table, network1, network2


def testDatabase():
    varNames = ('a', 'b', 'c')
    data = np.array([[1, 10, 100], [2, 10, 200], [1, 10, 100], [1, 10, 200]])
    db = database.TableDB(varNames, data)

    inst = db.instantiations()
    print(inst)

    return db


def testGsquared():
    g2 = stats.gSquared(db, 'a', 'b', {'c'})
    return g2


def parseSampleLearn(samples):
    print('Parsing...')
    start = time.time()
    network = io.parseBIF('data/alarm.bif')
    end = time.time()
    print('  ->{} seconds'.format(end - start))

    start = time.time()
    print('Sampling...')
    data = {varName: [] for varName in network.nodes()}
    for i in range(samples):
        sample = network.sample()

        for varName in data.keys():
            data[varName].append(sample[varName])

    end = time.time()
    print('  ->{} seconds'.format(end - start))

    for i in range(1000, samples + 1, 100):
        reduced = {var: data[var][:i] for var in data.keys()}
        table = database.TableDB(reduced.keys(), tuple(zip(*reduced.values())))

        #print('Learning with {} samples...'.format(i))
        start = time.time()
        newNetwork = Learning.MMHC(table)
        end = time.time()
        #print('  ->{} seconds'.format(end-start))
        print('{}\t{}'.format(i, end - start))

    return network, newNetwork


def testRank():
    data = []
    for x in np.linspace(0, 10, 100):
        a = (x + np.random.normal(0, 0.1))
        b = (x + np.random.normal(0, 0.1)) ** 2
        c = (x + np.random.normal(0, 0.1)) ** 3
        d = 5 + (x + np.random.normal(0, 0.1)) ** 0.5
        e = 5 - (x + np.random.normal(0, 0.1)) ** 0.5
        data.append([a, b, c, d, e])

    data = np.matrix(data)
    dataRanked = mstats.rankdata(data, axis=0)

    plt.plot(data[:, 0], data[:, 1])
    plt.plot(dataRanked[:, 0], dataRanked[:, 1])
    plt.show()

    plt.plot(data[:, 0], data[:, 2])
    plt.plot(dataRanked[:, 0], dataRanked[:, 2])
    plt.show()

    plt.plot(data[:, 3], data[:, 4])
    plt.plot(dataRanked[:, 3], dataRanked[:, 4])
    plt.show()

if __name__ == '__main__':
    #network, newNetwork = parseSampleLearn(1000)
    testRank()
