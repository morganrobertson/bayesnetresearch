import math

from scipy.spatial.distance import mahalanobis

import numpy as np
import scipy.stats

import util


def correlation(data, x, y):
    '''
    Calculate the Pearson correlation between two variables.

    data : A TableDB object containing the dataset.
    x, y : The variables to calculate the correlation for.
    '''

    corr, pVal = scipy.stats.pearsonr(data.getData(x), data.getData(y))
    return corr


@util.cache(lambda data, x, y, controlSet=None:
            (id(data), frozenset({x, y}),
             None if controlSet is None else frozenset(controlSet)))
def partialCorrelation(data, x, y, controlSet=None):
    '''
    Calculate the partial correlation between two continuous variables given a
    set of controlling variables.

    data : A TableDB object containing the dataset.
    x, y : The variables to calculate the correlation for.
    controlSet : The set of controlling variables. If not specified, all
        variables in the dataset besides x and y will be used as controlling
        variables.
    '''

    def partialCorrelationHelper(data, x, y, controlSet, cache):
        if len(controlSet) == 0:
            return correlation(data, x, y)

        controlSet = set(controlSet)  # Make a copy so we can modify it
        z = controlSet.pop()

        # Make keys to look up cached results
        r_xy_key = frozenset({x, y, frozenset(controlSet)})
        r_xz_key = frozenset({x, z, frozenset(controlSet)})
        r_yz_key = frozenset({y, z, frozenset(controlSet)})

        if len(controlSet) == 0:
            # Base case: calculate necessary correlations
            r_xy = (cache[r_xy_key] if r_xy_key in cache else
                    correlation(data, x, y))
            r_xz = (cache[r_xz_key] if r_xy_key in cache else
                    correlation(data, x, z))
            r_yz = (cache[r_yz_key] if r_yz_key in cache else
                    correlation(data, y, z))
        else:
            # Recursively calculate necessary partial correlations
            r_xy = (cache[r_xy_key] if r_xy_key in cache else
                    partialCorrelationHelper(data, x, y, controlSet, cache))
            r_xz = (cache[r_xz_key] if r_xz_key in cache else
                    partialCorrelationHelper(data, x, z, controlSet, cache))
            r_yz = (cache[r_yz_key] if r_yz_key in cache else
                    partialCorrelationHelper(data, y, z, controlSet, cache))

        # Account for rounding errors
        r_xy = -1.0 if r_xy < -1.0 else 1.0 if r_xy > 1.0 else r_xy
        r_xz = -1.0 if r_xz < -1.0 else 1.0 if r_xz > 1.0 else r_xz
        r_yz = -1.0 if r_yz < -1.0 else 1.0 if r_yz > 1.0 else r_yz

        # Update cache
        cache[r_xy_key] = r_xy
        cache[r_xz_key] = r_xz
        cache[r_yz_key] = r_yz

        try:
            return ((r_xy - r_xz * r_yz) /
                    (math.sqrt(1 - r_xz ** 2) * math.sqrt(1 - r_yz ** 2)))
        except ZeroDivisionError as e:
            print(r_xy)
            print(r_xz)
            print(r_yz)
            import pdb
            pdb.set_trace()
            pass

    if controlSet is None:
        controlSet = set(data.variables) - {x, y}

    cache = {}
    r = partialCorrelationHelper(data, x, y, controlSet, cache)

    # Account for rounding errors
    return -1.0 if r < -1.0 else 1.0 if r > 1.0 else r


def fisherZ(r):
    '''
    Perform a Fisher Z transformation.
    '''

    try:
        return math.log((1 + r) / (1 - r)) / 2
    except ValueError as e:
        return np.inf
    except ZeroDivisionError as e:
        return np.inf


def isPositiveDefinite(matrix):
    '''
    Determine whether a matrix is positive definite.

    matrix : The matrix to test.
    '''

    try:
        # Matrix is positive definite iff Cholesky decomposable
        np.linalg.cholesky(matrix)

        # Theoretically, the inverse of a PD matrix is PD, but we must also
        # check the inverse due to finite numerical precision
        inv = np.linalg.inv(matrix)
        np.linalg.cholesky(inv)

        # The matrix and its inverse should have positive determinants if they
        # are PD, but we must check due to finite numerical precision
        assert(np.linalg.det(matrix) > 0.0)
        assert(np.linalg.det(inv) > 0.0)

        return True

    except (np.linalg.LinAlgError, AssertionError):
        return False


#TODO: verify correctness
@util.breakOnResult(lambda x: np.isnan(x).any() or np.isinf(x).any())
def gaussianPdf(x, mean, cov):
    '''
    Calculate the density of a multivariate Gaussian distribution at a given
    point or points. The covariance matrix must be a symmetric positive-
    semidefinite matrix to guarantee that a valid result is returned.

    x : The point(s) at which to calculate the density. If this is a 1-D array
        (i.e. a single point), a scalar is returned. If this is a 2-D array
        (i.e. a vector of points), an array of values is returned.
    mean : The mean vector of the Gaussian distribution.
    cov : The covariance matrix of the Gaussian distribution.
    '''

    dimension = x.shape[x.ndim - 1]
    det = np.linalg.det(cov)
    assert(det > 0.0)
    invCov = np.linalg.inv(cov)
    diff = x - mean
    prod = diff.dot(invCov).dot(diff.T)
    exponent = -0.5 * (prod if x.ndim == 1 else np.diag(prod))
    return (1 / math.sqrt((2 * math.pi) ** dimension * det) *
            np.exp(exponent))


def gaussianMixtureLikelihood(data, means, covs, priors):
    '''
    Calculate the likelihood of a Gaussian mixture model with respect to the
    data.

    data : A TableDB object containing the dataset.
    means : The mean vectors of the components.
    covs : The covariance matrices of the components.
    priors : The priors of the components.
    '''

    likelihoods = np.zeros(len(data))
    for mean, cov, prior in zip(means, covs, priors):
        likelihoods += prior * gaussianPdf(data.data, mean, cov)

    return np.sum(np.log(likelihoods))


def networkToGaussian(network, order=None, evidence=None):
    '''
    Obtain the parametric representation of a linear Gaussian Bayesian network,
    which is equivalent to a multivariate Gaussian distribution. Returns the
    mean vector and covariance matrix of the equivalent distribution along with
    a sequence of variables corresponding to the entries of the mean vector and
    covariance matrix.

    network : A Bayesian network where the conditional probability distribution
        of each node is a conditional linear Gaussian distribution.
    order : An ordering of variables used to order entries in the mean vector
        and covariance matrix.
    evidence : Observed data to be applied

    References
    ----------
    (Shachter and Kenley, 1989)
    '''

    nNodes = len(network)
    mean = np.zeros(nNodes)
    cov = np.zeros(shape=(nNodes, nNodes))
    sortedNodes = network.topologicalSort()
    idx = {node: i for i, node in enumerate(sortedNodes)}

    for i, node in enumerate(sortedNodes):
        cpd = network.getCPD(node)

        for j in range(i):
            cov[i, j] = cov[j, i] = sum(cov[idx[parent], j] * w for parent, w
                                        in zip(cpd.parents, cpd.weights))

        cov[i, i] = cpd.variance + sum(cov[idx[parent], i] * w for parent, w
                                       in zip(cpd.parents, cpd.weights))

        parentMeans = np.array([mean[idx[parent]] for parent in cpd.parents])
        mean[i] = cpd.conditionalMean(parentMeans)

    if order is not None:
        orderIdx = [idx[var] for var in order]
        mean = mean[orderIdx]
        cov = cov[orderIdx][:, orderIdx]
        sortedNodes = order

    return mean, cov, sortedNodes


def spatialNetworkToGaussian(network, order=None, coords=None, kernel=None,
                             evidence=None):
    '''
    evidence : A dictionary that maps variable names to evidence lists. Each
        evidence list should contain 2-tuples where the first value is the
        observed value and the second value is the location of the observation.
    evidence : A sequence of dictionaries that map variable names to observed
        values. Each dictionary must contain a 'coords' key that specifies the
        spatial location of the observation.
    '''

    nNodes = len(network) + sum(len(inst) for inst in evidence)
    mean = np.zeros(nNodes)
    cov = np.zeros(shape=(nNodes, nNodes))
    sortedNodes = network.topologicalSort()
    varNames = []
    idx = {}
    i = 0

    for node in sortedNodes:
        idx[node] = i  # Map node to mean/covariance index
        cpd = network.getCPD(node)

        # Set parameters for non-evidence node
        for j in range(i + 1):
            for parent, w in zip(cpd.parents, cpd.weights):
                if parent in evidence:
                    k = np.array([kernel(coords, coords)] +
                                 [kernel(coords, x) for (val, x) in
                                  evidence[parent] if coords != x])
                    k = w * k / sum(k)
                    # TODO: k may be NaN
                else:
                    k = np.array([w])

                parentIdx = idx[parent]
                covs = cov[parentIdx: parentIdx + len(k), j]
                cov[i, j] += np.dot(k, covs)
                cov[j, i] = cov[i, j]

        cov[i, i] += cpd.variance

        parentMeans = np.array([mean[idx[parent]] for parent in cpd.parents])
        mean[i] = cpd.weight0 + np.dot(parentMeans, cpd.weights)

        varNames.append(node)
        i += 1

        # Set parameters for evidence nodes
        if node in evidence:
            for val, x in evidence[node]:
                if coords != x:
                    for j in range(i):
                        cov[i, j] = cov[j, i] = 0
                    cov[i, i] = cov[i - 1, i - 1]
                    mean[i] = mean[i - 1]

                    varNames.append(node + str(x))
                    i += 1

    # Condition on evidence
    inst = {}
    for varName, evidenceList in evidence.items():
        inst.update({varName + (str(x) if x != coords else ''): val
                     for val, x in evidenceList})
    g = Gaussian(mean, cov, varNames).condition(inst)

    # Reorder mean and covariance according to specified order
    if order is not None:
        orderIdx = [idx[var] for var in order]
        mean = g.mean[orderIdx]
        cov = g.covariance[orderIdx][:, orderIdx]
        sortedNodes = order
    else:
        mean = g.mean
        cov = g.covariance

    return mean, cov, sortedNodes


class Gaussian():
    '''
    Representation of a multivariate Gaussian distribution.
    '''

    def __init__(self, mean=None, covariance=None, varNames=None,
                 network=None, data=None):
        if network is not None:
            self._mean, self._cov, self._varNames = networkToGaussian(network,
                                                                      varNames)
            self._varNames = tuple(self._varNames)
        elif data is not None:
            self._cov = data.covariance()
            self._mean = data.mean()
            self._varNames = tuple(varNames)
        else:
            if varNames is None:
                raise ValueError('Names of variables must be specified.')

            self._varNames = tuple(varNames)
            nVars = len(varNames)

            if mean is None:
                self._mean = np.zeros(nVars)
            else:
                self._mean = np.array(mean)

            if covariance is None:
                self._cov = np.eye(nVars)
            else:
                self._cov = np.array(covariance)

    @property
    def mean(self):
        return self._mean

    @property
    def covariance(self):
        return self._cov

    @property
    def variables(self):
        return self._varNames

    def pdf(self, x):
        '''
        Calculate the probability density at a given point.

        x : Either an array of values or a mapping containing a full
            instantiation corresponding to the variables modeled by this
            distribution.
        '''

        if type(x) is dict:
            x = np.array([x[varName] for varName in self._varNames])

        dimension = len(self._varNames)
        det = np.linalg.det(self._cov)
        assert(det > 0.0)
        invCov = np.linalg.inv(self._cov)
        diff = x - self._mean
        exponent = -0.5 * diff.dot(invCov).dot(diff)
        return (1 / math.sqrt((2 * math.pi) ** dimension * det) *
                math.exp(exponent))

    def mahalanobisDistance(self, x):
        '''
        Calculate the Mahalanobis distance between a point and the mean of this
        distribution.

        x : Either an array of values or a mapping containing a full
            instantiation corresponding to the variables modeled by this
            distribution.
        '''

        if type(x) is dict:
            x = np.array([x[varName] for varName in self._varNames])

        invCov = np.linalg.inv(self._cov)
        return mahalanobis(x, self._mean, invCov)

    def partition(self, varNames):
        '''
        Extract a multivariate Gaussian distribution corresponding to the
        specified variables. Essentially, a new Gaussian is created by copying
        the mean vector and covariance matrix, ignoring any variables not
        contained in varNames. This is equivalent to marginalizing over all
        variables not in varNames.

        varNames : The collection of variables to retain. If this is a
            sequence, the order of entries in the mean vector and covariance
            matrix of the resulting distribution will correspond to the order
            of variable names in the sequence.
        '''

        varNames = tuple(varNames)
        idx = [self._varNames.index(varName) for varName in varNames]

        mean = self._mean[idx]
        cov = self._cov[idx, :][:, idx]
        return Gaussian(mean, cov, varNames)

    def marginalize(self, varNames):
        '''
        Perform marginalization over a set of variables.

        varNames : The set of variables to eliminate.

        References
        ----------
        (Koller and Friedman, 2009): 7.1.2
        '''

        if len(varNames) == 0:
            return self

        varNames = set(varNames)
        idx = [i for i, varName in enumerate(self._varNames)
               if varName not in varNames]
        newVars = tuple(varName for varName in self._varNames
                        if varName not in varNames)

        mean = self._mean[idx]
        cov = self._cov[idx, :][:, idx]
        return Gaussian(mean, cov, newVars)

    def condition(self, inst):
        '''
        Perform conditioning given a set of instantiated variables.

        inst : An instantiation of variables (i.e. a mapping from variable
            names to values).

        References
        ----------
        (Sung, 2004): 4.1
        (Mardia et al., 1979): Multivariate Analysis
        '''

        if len(inst) == 0:
            return self

        # Calculate parameters for x2 given x1 where x2 is the set of
        # unobserved variables and x1 is the set of observed variables

        x1_idx = [i for i, varName in enumerate(self._varNames)
                  if varName in inst.keys()]
        x2_idx = [i for i, varName in enumerate(self._varNames)
                  if varName not in inst.keys()]
        newVars = tuple(varName for varName in self._varNames
                        if varName not in inst.keys())

        x1 = np.array([inst[v] for v in self._varNames if v in inst.keys()])
        mean_1 = self._mean[x1_idx]
        mean_2 = self._mean[x2_idx]
        cov_11_inv = np.linalg.inv(self._cov[x1_idx, :][:, x1_idx])
        cov_12 = self._cov[x1_idx, :][:, x2_idx]
        cov_21 = self._cov[x2_idx, :][:, x1_idx]
        cov_22 = self._cov[x2_idx, :][:, x2_idx]

        mean = mean_2 + cov_21.dot(cov_11_inv).dot(x1 - mean_1)
        cov = cov_22 - cov_21.dot(cov_11_inv).dot(cov_12)
        return Gaussian(mean, cov, newVars)

    def inference(self, query, inst):
        '''
        query : The set of query variables.
        inst : An instantiation of observed variables (i.e. a mapping from
            variable names to values).
        '''

        eliminate = set(self._varNames) - set(query)
        return self.condition(inst).marginalize(eliminate)
