import itertools
import logging
import random
import time
from functools import wraps

import numpy as np


def subsets(iterable, r=None):
    '''
    Get all subsets of an iterable of size r or smaller.

    iterable : An iterable to produce subsets of.
    r : The maximum size of subsets produced.
    '''

    if r is None:
        r = len(iterable)

    for l in range(1, r + 1):
        for s in itertools.combinations(iterable, l):
            yield set(s)


def choice(items, weights=None):
    '''
    Select an item with probability proportional to its relative weight.

    sequence : A sequence to select an item from.
    weights : Weights corresponding to each item in the sequence. If not given,
        a uniform distribution is used.
    '''

    if weights is None:
        weights = [1.0 for i in range(len(items))]

    rand = np.random.uniform(0, sum(weights))
    total = 0

    for item, weight in zip(items, weights):
        total += weight

        if rand <= total:
            return item


def randCov(dim):
    while True:
        try:
            cov = np.random.randn(dim, dim)
            cov = cov * cov.T
            np.linalg.cholesky(cov)
            return cov
        except:
            pass


def generateGMMData(nSamples, dim, nComponents, means=None, covs=None,
                    priors=None):

    if means is None:
        means = [np.random.randn(dim) for i in range(nComponents)]
    else:
        if nComponents != len(means):
            raise ValueError('The number of given mean vectors must be equal '
                             'to the number of components.')

    if covs is None:
        covs = [randCov(dim) for i in range(nComponents)]
    else:
        if nComponents != len(covs):
            raise ValueError('The number of given covariance matrices must be '
                             'equal to the number of components.')

    if priors is None:
        priors = np.ones(nComponents) / nComponents
    else:
        if nComponents != len(priors):
            raise ValueError('The number of given priors must be equal to the '
                             'number of components.')

    data = np.zeros((nSamples, dim))
    for i in range(nSamples):
        cmpnt = np.random.choice(range(nComponents), p=priors)
        data[i] = np.random.multivariate_normal(means[cmpnt], covs[cmpnt])

    return data, means, covs, priors


def cache(key=None):
    '''
    Decorator that caches results returned by the decorated function. If a key
    function is given, the key function is called with the parameters passed to
    the decorated function each time the decorated function is called. The
    return value of the key function is then used to look up previously
    computed results stored in the cache. If no key function is specified, a
    tuple consisting of the positional arguments and a set consisting of (key,
    value) pairs from the keyword arguments are used as lookup keys.

    key : A function that takes the same arguments as the decorated function
        and returns a hashable object used to look up results in the cache.
    '''

    def decorator(func):
        kache = {}
        hit = 0
        miss = 0

        @wraps(func)
        def wrapper(*args, **kwargs):
            nonlocal kache
            nonlocal hit
            nonlocal miss

            if key is None:
                k = (args, frozenset(kwargs.items()))
            else:
                k = key(*args, **kwargs)

            if k in kache:
                hit += 1
            else:
                miss += 1

            result = kache[k] if k in kache else func(*args, **kwargs)
            kache[k] = result

            return result
        return wrapper
    return decorator


def logged(func):
    '''
    Decorator that logs execution information about a function.

    func : The function to be decorated.
    '''

    # The number of times the wrapped function has been called
    nCalls = 0

    @wraps(func)
    def wrapper(*args, **kwargs):
        nonlocal nCalls
        logger = logging.getLogger(func.__module__)

        logger.debug('[{}] Calling {}()'.format(nCalls, func.__name__))
        callNumber = nCalls
        nCalls = nCalls + 1
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        logger.debug('[{}] Exiting {}()\t[{} seconds]'.format(callNumber,
                     func.__name__, round(end - start, 4)))

        return result
    return wrapper


def logParamTypes(func):
    '''
    Decorator that logs the types of parameters passed to a function and the
    type of the function's return value.

    func : The function to be decorated.
    '''

    from itertools import chain

    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = logging.getLogger(func.__module__)

        logger.debug('Calling {}() with parameter types:'.
                     format(func.__name__))

        annotations = func.__annotations__
        varnames = func.__code__.co_varnames
        for name, val in chain(zip(varnames, args), kwargs.items()):
            if name in annotations:
                out = '\t{} ({}):\t{}'.format(name, annotations[name],
                                              type(val))
            else:
                out = '\t{}:\t{}'.format(name, type(val))

            logger.debug(out)

        result = func(*args, **kwargs)
        logger.debug('Exiting {}() with return type \'{}\''.
                     format(func.__name__, type(result)))

        return result
    return wrapper


def breakOnEnter(func):
    '''
    Decorator that causes debug mode to be entered when the decorated function
    is called.

    func : The function to be decorated.
    '''

    import pdb

    @wraps(func)
    def wrapper(*args, **kwargs):
        result = pdb.runcall(func, *args, **kwargs)
        return result
    return wrapper


def breakOnExit(func):
    '''
    Decorator that causes debug mode to be entered when the decorated function
    returns.

    func : The function to be decorated.
    '''

    import pdb
    import sys

    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        pdb.Pdb().set_trace(sys._getframe().f_back)

        return result
    return wrapper


def breakOnResult(compare):
    '''
    Decorator that causes debug mode to be entered when the decorated function
    returns a certain result.

    compare : A function to perform the comparison. When the decorated function
        returns, this function is called with the result. Debug mode is entered
        if the function call evaluates to True.
    '''

    import pdb
    import sys

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)

            if compare(result):
                pdb.Pdb().set_trace(sys._getframe().f_back)

            return result

        return wrapper
    return decorator


def breakOnException(exceptionList):
    '''
    Decorator that causes debug mode to be entered when the decorated function
    throws an exception contained in the given list.

    exceptionList : A tuple of exceptions to break on.
    '''

    import pdb
    import sys

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
                return result
            except exceptionList as e:
                pdb.Pdb().set_trace(sys._getframe().f_back)
                raise e
        return wrapper
    return decorator
