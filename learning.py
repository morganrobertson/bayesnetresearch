import itertools
import math

import networkx as nx
import numpy as np
from scipy import special

import bayesnet as bn
import cpd
import cstats
import database
import stats
import util


def linearRegression(data, indVars, depVars):
    '''
    Learn a linear regression model for a dataset.
    '''

    ones = np.ones((len(data), 1))
    x = np.concatenate((ones, data.subtable(indVars)), axis=1)
    y = data.subtable(depVars)
    beta, *junk = np.linalg.lstsq(x, y)

    return beta.flatten()


def estimateParams(network, data):
    '''
    Estimate the parameters of a Bayesian network given a network structure and
    data.

    network : A BayesNet object that represents a network skeleton, i.e. the
        independence relationships (edges) between variables have been learned,
        but the CPDs for the variables have not. The nodes in the network must
        correspond to the variable names in the TableDB object passed to this
        function.
    data : A TableDB object containing data to be used for estimating
        parameters.
    '''

    if data.datatype == database.DISCRETE:
        discreteMLE(network, data)
    elif data.datatype == database.CONTINUOUS:
        gaussianMLE(network, data)
    else:
        raise ValueError('Data must be either discrete or continuous')


def discreteMLE(network, data):
    '''
    Given a skeleton graph structure and a discrete multinomial dataset,
    use maximum likelihood estimation to create the conditional probability
    distributions (CPDs) in the network.

    network : A BayesNet object that represents a network skeleton, i.e. the
        independence relationships (edges) between variables have been learned,
        but the CPDs for the variables have not. The nodes in the network must
        correspond to the variable names in the TableDB object passed to this
        function.
    data : A TableDB object containing data to be used for estimating
        parameters.
    '''

    # Map variables to their respective values for each instance
    dataDict = data.toDict()

    # Map variables to their possible values
    varVals = data.valuesDict()

    #print('dataDict:\t{}'.format(dataDict))
    #print('varVals:\t{}'.format(varVals))

    for child in data.variables:
        parents = network.getParents(child)
        varSet = parents + [child]

        #print('Parents: {}'.format(parents))
        #print('Varset:  {}'.format(varSet))

        # Generate all possible instantiations of the parent variables
        parentInst = itertools.product(*[[(var, val) for val in varVals[var]] for var in parents])

        probs = {}
        for instantiation in parentInst:
            smalldata = list(zip(*[[(var, val) for val in dataDict[var]] for var in varSet]))
            #print('smalldata:\t{}'.format(smalldata))

            # TODO: database can count things
            counts = {}
            totalCount = 0
            for val in varVals[child]:
                fullInst = instantiation + ((child, val),)
                #print('full: {}'.format(fullInst))
                count = smalldata.count(fullInst)
                counts[fullInst] = count
                totalCount += count

            probs.update({frozenset(inst): count / totalCount
                         for inst, count in counts.items()})

        t = cpd.TableCPD(child, parents, probs)
        network.updateCPD(child, t)


def gaussianMLE(network, data):
    '''
    Given a skeleton graph structure and a dataset consisting of continuous
    normally distributed variables with linear relationships, use maximum
    likelihood estimation to create the conditional probability distributions
    (CPDs) in the network.

    network : A BayesNet object that represents a network skeleton, i.e. the
        independence relationships (edges) between variables have been learned,
        but the CPDs for the variables have not. The nodes in the network must
        correspond to the variable names in the TableDB object passed to this
        function.
    data : A TableDB object containing data to be used for estimating
        parameters.

    References
    ----------
    (Koller and Friedman, 2009): Theorem 7.4; 17.2.4

    '''

    # Calculate the mean vector for the dataset
    means = data.mean()

    # Calculate the covariance matrix for the dataset
    covariance = data.covariance()

    # Map variable names to their indices
    idx = {varName: index for index, varName in enumerate(data.variables)}

    for child in data.variables:
        parents = network.getParents(child)
        childIdx = idx[child]
        parentIdx = [idx[parent] for parent in parents]

        #print('child:\t{}'.format(child))
        #print('parents:\t{}'.format(parents))

        childMean = means[childIdx]
        parentMeans = means[parentIdx]

        # TODO: use np.array
        # TODO: use database.covariance()
        if len(parents) > 0:
            # Calculate necessary covariance sub-matrices
            cov_xx = covariance[[childIdx], :][:, [childIdx]]
            cov_xU = covariance[[childIdx], :][:, parentIdx]
            cov_Ux = covariance[parentIdx, :][:, [childIdx]]
            cov_UU = covariance[parentIdx, :][:, parentIdx]

            # Calculate weights and variance
            beta_0 = np.double(childMean - cov_xU.dot(np.linalg.inv(cov_UU)).
                               dot(parentMeans))
            beta = np.linalg.inv(cov_UU).dot(cov_Ux).reshape((-1,))
            variance = np.double(cov_xx - (cov_xU.dot(np.linalg.inv(cov_UU)).
                                 dot(cov_Ux)))

        else:
            beta_0 = childMean
            beta = []
            variance = covariance[childIdx, childIdx]

        c = cpd.LinearGaussianCPD(child, parents, beta_0, beta, variance)
        network.updateCPD(child, c)


@util.logged
def gaussianEM(data, k, iterations=100, means=None, covs=None):
    '''
    Perform expectation maximization to learn the parameters of a multivariate
    Gaussian mixture model.

    data : A TableDB object containing data to be used for estimating
        parameters.
    k : The number of Gaussians components that will be contained in the
        mixture model.
    means : An optional sequence of mean vectors that will be used to
        initialize the mean vector for each component. The number of mean
        vectors given must be equal to k and the length of each vector must be
        equal to the number of variables in the given dataset.
    covs : An optional sequence of covariance matrices that will be used to
        initialize the covariance matrix for each component. The number of
        covariance matrices given must be equal to k and the dimension of each
        matrix must be equal to the number of variables in the given dataset.
    '''

    nVars = len(data.variables)
    m = len(data)

    # Initialize mean vectors
    if means is None:
        means = np.array([data.sample() for i in range(k)])
    else:
        means = np.array(means)

        if len(means) != k:
            raise ValueError('The number of given mean vectors must be equal '
                             'to k.')
        for i in range(k):
            if len(means[i]) != nVars:
                raise ValueError('The length of each given mean vector must '
                                 'be equal to the number of variables in the '
                                 'given dataset.')

    # Initialize covariance matrices
    if covs is None:
        covs = np.array([np.eye(nVars) for i in range(k)])
    else:
        covs = np.array(covs)

        if len(covs) != k:
            raise ValueError('The number of given covariance matrices must be '
                             'equal to k.')
        for i in range(k):
            if covs[i].shape != (nVars, nVars):
                raise ValueError('The dimension of each given covariance '
                                 'matrix must be equal to the number of '
                                 'variables in the given dataset.')

    # Initialize the weights for each data instance
    dataWeights = np.ones((k, m)) / m

    # Initialize priors of each cluster
    clusterPriors = np.ones(k) / k

    for iteration in range(iterations):
        # E step
        likelihoods = np.zeros((k, m))
        for i in range(k):
            # Ensure that covariance matrices are positive definite
            if not cstats.isPositiveDefinite(covs[i]):
                raise ValueError('Expectation maximization failed with given'
                                 'parameters')

            likelihoods[i] = cstats.gaussianPdf(data.data, means[i], covs[i])

        # Calculate normalized weights
        # Note: The numerator in this calculation is a (k by m) array and the
        # denominator is a (1 by m) array.
        dataWeights = (np.diagflat(clusterPriors).dot(likelihoods) /
                       clusterPriors.dot(likelihoods))

        # NaNs may have been produced in the previous calculation for instances
        # whose (prior * likelihood) is zero for all clusters. The weights for
        # these instances should be zero.
        dataWeights = np.nan_to_num(dataWeights)

        # M step
        clusterPriors = np.sum(dataWeights, axis=1) / np.sum(dataWeights)

        for i in range(k):
            weightedData = np.diagflat(dataWeights[i]).dot(data.data)
            means[i] = np.sum(weightedData, axis=0) / np.sum(dataWeights[i])

            diff = data.data - means[i]
            covs[i] = (np.sum(dataWeights[i][j] *
                              np.outer(diff[j], diff[j])
                              for j in range(m)) /
                       np.sum(dataWeights[i]))

    # Ensure that covariance matrices are positive definite
    for cov in covs:
        if not cstats.isPositiveDefinite(cov):
            raise ValueError('Expectation maximization failed with given'
                             'parameters')

    return means, covs, clusterPriors, dataWeights


def calculateScore(network, data):
    '''
    Calculate the structure score for a Bayesian network given data. An
    appropriate scoring metric is chosen based on the type of the data.

    network : The Bayesian network to score.
    data : A TableDB containing observation data.
    '''

    if data.datatype == database.DISCRETE:
        return calculateBIC(network, data)
    elif data.datatype == database.CONTINUOUS:
        return calculateBGe(network, data)
    else:
        raise ValueError('Data must be either discrete or continuous')


def calculateLocalScore(child, parents, data, cache=None):
    '''
    Calculate the structure score for a single variable in a Bayesian network
    given its parents and data. An appropriate scoring metric is chosen based
    on the type of the data.

    child : A child node for which to calculate the score.
    parents : The parent nodes of child.
    data : A TableDB containing observation data.
    cache : A dict to be used for caching results for subsequent calls.
    '''

    if data.datatype == database.DISCRETE:
        return calculateLocalBIC(child, parents, data, cache)
    elif data.datatype == database.CONTINUOUS:
        return calculateLocalBGe(child, parents, data, cache)
    else:
        raise ValueError('Data must be either discrete or continuous')


def calculateBIC(network, data):
    '''
    Calculate the BIC score for a Bayesian network given observed data.

    network : The Bayesian network to score.
    data : A TableDB containing observation data.
    '''

    m, n = data.size()
    score = 0    # The BIC score of the network

    for child, cpd in network:
        score += calculateLocalBIC(cpd, data)

    return score


#TODO: take child, parents, data
def calculateLocalBIC(cpd, data, cache=None):
    '''
    Calculate the BIC score for a single variable given its conditional
    probability distribution and observed data. The parent and child variables
    used for calculating the BIC are determined from the CPD.

    cpd : The CPD for the node whose score is being calculated.
    data : A TableDB containing observation data.
    cache : A dict to be used for caching results for subsequent calls.
    '''
    #TODO: does this need to be conditioned?
    #TODO: only get data dict for necessary vars

    #TODO: cache
    if cache is None:
        cache = {}

    m, n = data.size()
    score = 0

    # Map variables to their respective values for each instance
    dataDict = data.toDict()

    # Calculate the log-likelihood of the parameters in the CPD with respect
    # to the data
    for i in range(m):
        query = {var: dataDict[var][i] for var in cpd.variables}
        theta = cpd.prob(query)
        score += math.log(theta)

    # Penalize complexity
    score -= 0.5 * math.log(m) * cpd.countParameters()

    return score


def calculateBGe(network, data):
    '''
    Calculate the log of the BGe score for a linear Gaussian network given the
    network structure and data. The data is assumed to have been generated by
    a multivariate normal distribution.

    network : The Bayesian network to score.
    data : A TableDB containing observation data.
    '''

    cache = {}

    score = 0
    for node in network.nodesIter():
        parents = network.getParents(node)
        score += calculateLocalBGe(node, parents, data, cache)

    return score


def calculateLocalBGe(child, parents, data, cache=None):
    '''
    Calculate the log of the local BGe score for a child node given its parents
    and data. The child and parents are assumed to be part of a linear Gaussian
    network and the data is assumed to have been generated by a multivariate
    normal distribution.

    child : A child node for which to calculate the BGe score.
    parents : The parent nodes of child.
    data : A TableDB containing observation data.
    '''

    # TODO: Remove computation for mean and cov of subdata (modify database.py)

    if cache is None:
        cache = {}

    def c(n, alpha):
        return -((alpha * n / 2) * math.log(2) +
                 (n * (n - 1) / 4) * math.log(math.pi) +
                 sum([special.gammaln((alpha + 1 - i) / 2)
                      for i in range(1, n + 1)]))

    def p_D_Gs(varNames):
        if len(varNames) == 0:
            return 1.0

        m, n = len(data), len(varNames)

        # Get the data table corresponding to the specified variables
        subdata = data.subtable(varNames)

        # Calculate sample mean and sample covariance
        subdata_mean = np.matrix(np.mean(subdata, axis=0)).T
        subdata_err = (m - 1) * np.matrix(np.cov(subdata, rowvar=0))

        # Use uninformed prior mean and precision
        mean_0 = np.matrix(np.zeros(n)).T
        T_0 = np.matrix(np.identity(n))  # TODO: multiply by constants?
        alpha = n + 2
        v = 1

        # Calculate posterior precision matrix
        T_m = (T_0 + subdata_err + ((v * m) / (v + m)) *
               (mean_0 - subdata_mean) * (mean_0 - subdata_mean).T)

        # Calculate the log likelihood of the data given the network structure
        p = ((-n * m / 2) * math.log(2 * math.pi) +
             (n / 2) * math.log(v / (v + m)) +  # TODO: Scott has 1+v+m in denominator
             c(n, alpha) - c(n, alpha + m) +
             (alpha / 2) * math.log(np.linalg.det(T_0)) +
             (-(alpha + m) / 2) * math.log(np.linalg.det(T_m)))

        return p

    parents = frozenset(parents)
    variables = frozenset(itertools.chain((child,), parents))

    # Compute likelihoods
    p1 = cache[variables] if variables in cache else p_D_Gs(variables)
    p2 = cache[parents] if parents in cache else p_D_Gs(parents)

    # Update cache
    cache[variables] = p1
    cache[parents] = p2

    #print('Score:\t{}\tp1={}\tp2={}'.format(p1 / p2, p1, p2))
    return p1 - p2


def hillClimbing(data, network=None, maxNetwork=None, tol=1e-10):
    '''
    Perform a hill climbing search to find the highest scoring network
    structure.

    data : A TableDB containing observation data.
    network : A prior network structure to begin the search from. If specified,
        this network must contain exactly one node for each variable in data.
    maxNetwork : A network structure constraining which edges can be added to
        the network during the search.
    tol : The tolerance value for comparing the previous best score to the
        current best score.
    '''

    # network and maxNetwork are read-only and should not be modified

    # Cache for scores
    cache = {}

    # Create an empty structure if none is given
    if network is None:
        network = bn.BayesNet()
        for var in data.variables:
            network.addNode(var)
    elif set(network.nodes()) != set(data.variables):
        raise ValueError('Network and data must contain the same variables.')

    # Create a full structure for maxNetwork if none is given
    if maxNetwork is None:
        maxNetwork = nx.DiGraph()
        maxNetwork.add_edges_from([(v1, v2) for v1 in data.variables
                                   for v2 in data.variables if v1 != v2])
    elif set(maxNetwork.nodes()) != set(data.variables):
        raise ValueError('maxNetwork and data must contain the same '
                         'variables.')

    while True:
        # The search adds, deletes, or reverses an edge, then recomputes the
        # score. Decomposability of the score is used to calculate the change
        # in the score at the affected node(s). This prevents needing to
        # recompute the score for the entire network.

        bestScore = 0
        bestNetwork = None

        # Try deleting edges
        for parent, child in network.edgesIter():
            # Calculate the local score before the edge is removed
            parents = network.getParents(child)
            oldScore = calculateLocalScore(child, parents, data, cache)

            # Calculate the local score after the edge is removed
            parents.remove(parent)
            newScore = calculateLocalScore(child, parents, data, cache)

            deltaScore = newScore - oldScore

            if deltaScore > bestScore:
                bestScore = deltaScore
                bestNetwork = bn.BayesNet(network)
                bestNetwork.removeEdge(parent, child)
                #print('HC: Remove {} -> {}\t{}'.format(parent, child, deltaScore))

        # Try adding edges
        edges = set(maxNetwork.edges()) - set(network.edges())
        for parent, child in edges:
            # Calculate the local score before the edge is added
            parents = network.getParents(child)
            oldScore = calculateLocalScore(child, parents, data, cache)

            # Calculate the local score after the edge is added
            parents.append(parent)
            newScore = calculateLocalScore(child, parents, data, cache)

            deltaScore = newScore - oldScore

            if deltaScore > bestScore:
                modifiedNetwork = bn.BayesNet(network)
                modifiedNetwork.addEdge(parent, child)

                # Make sure there are no cycles
                if modifiedNetwork.isDAG():
                    bestScore = deltaScore
                    bestNetwork = modifiedNetwork
                    #print('HC: Add {} -> {}\t{}'.format(parent, child, deltaScore))

        # Try reversing edges
        for parent, child in network.edgesIter():
            # Ensure that reversing the edge is a valid operation
            if (child, parent) not in maxNetwork.edges():
                continue

            # Calculate the local score at the child before the edge is
            # reversed
            parents = network.getParents(child)
            oldChildScore = calculateLocalScore(child, parents, data, cache)

            # Calculate the local score at the old child after the edge is
            # reversed
            parents.remove(parent)
            newChildScore = calculateLocalScore(child, parents, data, cache)

            # Calculate the local score at the parent before the edge is
            # reversed
            parents = network.getParents(parent)
            oldParentScore = calculateLocalScore(parent, parents, data, cache)

            # Calculate the local score at the old parent after the edge is
            # reversed
            parents.append(child)
            newParentScore = calculateLocalScore(parent, parents, data, cache)

            deltaScore = (newChildScore + newParentScore -
                          oldChildScore - oldParentScore)

            if deltaScore > bestScore:
                #print('Delta:\t{}'.format(deltaScore))
                modifiedNetwork = bn.BayesNet(network)
                modifiedNetwork.reverseEdge(parent, child)

                # Make sure there are no cycles
                if modifiedNetwork.isDAG():
                    bestScore = deltaScore
                    bestNetwork = modifiedNetwork
                    #print('HC: Rev {} -> {}\t{}'.format(parent, child, deltaScore))

        #print('Best score:\t{}'.format(bestScore))
        if bestScore <= tol:
            # Local maximum found, so terminate
            break
        else:
            #print('Previous score:\t{}'.format(calculateScore(network, data)))
            network = bestNetwork
            #print('network:\n{}\n{}\n'.format(network.nodes(), network.edges()))

    return network


def MMHC(data, network=None, maxNetwork=None):
    '''
    Perform Max-Min Hill-Climbing search. First, the MMPC algorithm (a
    constraint-based algorithm) is used to identify possible edges in the
    network. Next, hill climbing search is used to add and orient edges
    starting from an empty (or specified) structure, subject to the constraints
    found by MMPC.

    data : A TableDB containing observation data.
    network : A prior network structure to begin the search from. If specified,
        this network must contain exactly one node for each variable in data.
    maxNetwork : A network structure constraining which edges can be added to
        the network during the search.
    '''

    # Create a full structure for maxNetwork if none is given
    if maxNetwork is None:
        maxNetwork = nx.DiGraph()
        maxNetwork.add_edges_from([(v1, v2) for v1 in data.variables
                                   for v2 in data.variables if v1 != v2])
    elif set(maxNetwork.nodes()) != set(data.variables):
        raise ValueError('maxNetwork and data must contain the same '
                         'variables.')

    # Create an empty structure if none is given
    if network is None:
        network = bn.BayesNet()
        for var in data.variables:
            network.addNode(var)
    elif set(network.nodes()) != set(data.variables):
        raise ValueError('Network and data must contain the same variables.')

    structure = bn.BayesNet()
    for var in data.variables:
        structure.addNode(var)

    cache = {}  # Store intermediate results to avoid recomputing
    for x in data.variables:  # TODO: use only variables in network?
        neighbors = MMPC(x, data, cache)
        #print('neighbors({}):\t{}'.format(x, neighbors))

        for neighbor in neighbors:
            #TODO: add opposite edge too?
            if (x, neighbor) in maxNetwork.edges():
                structure.addEdge(x, neighbor)

    return hillClimbing(data, network, structure)


def MMPC(target, data, cache=None):
    '''
    The Max-Min Parents and Children algorithm. For a given target variable,
    determine the set of neighbors of the target.

    cache : A dict to be used for caching results for subsequent calls of MMPC.
    '''

    if cache is None:
        cache = {}

    # Check the cache for the CPC set of the target variable. If not in the
    # cache, calculate it and update the cache.
    cpc = cache[target] if target in cache else _MMPC(target, data)
    cache[target] = cpc

    # Remove non-symmetric parents and children
    for x in set(cpc):  # Copy cpc to allow removals in loop
        xCpc = cache[x] if x in cache else _MMPC(x, data)
        cache[x] = xCpc

        if target not in xCpc:
            cpc.remove(x)

    return cpc


def _MMPC(target, data):
    '''
    A helper function for MMPC. Returns a superset of parents and children of a
    target variable. Assuming faithfulness and perfect statistical tests, there
    will be no false negatives; that is, all true parents and children will be
    contained in the set returned by this function.
    Adapted from (Tsamardinos et al. 2006).
    '''

    cpc = set()     # Set of candidate parents and children
    hasChanged = True

    # Forward phase: Add variables to conditioning set.
    while hasChanged:
        var, association = maxMinHeuristic(target, cpc, data)

        if association > 0:
            cpc.add(var)
        else:
            hasChanged = False

    #print('_MMPC(fwd): cpc({})={}'.format(target, cpc))

    # Backward phase: Remove false positives from conditioning set.
    for x in set(cpc):  # Copy cpc to allow removals in loop
        for subset in util.subsets(cpc - {x}):
            if stats.independent(data, x, target, subset):
                #print('ind({}, {} | {})'.format(x, target, subset))
                cpc.remove(x)
                break

    #print('_MMPC(back): cpc({})={}'.format(target, cpc))
    return cpc


def maxMinHeuristic(target, cpc, data):
    '''
    Determine which variable maximizes the minimum association with the target
    variable relative to CPC (the candidate parents and children).
    Adapted from (Tsamardinos et al. 2006).

    target : The target variable.
    cpc : The CPC set (candidate parents and children).
    data : A TableDB containing observation data.
    '''

    maxMinAssoc = 0
    maxVar = None

    for x in data.variables:
        if x == target or x in cpc:
            continue

        minAssoc = stats.minAssociation(data, x, target, cpc)
        #print('minAssoc:\t{}'.format(minAssoc))

        if minAssoc > maxMinAssoc:
            maxMinAssoc = minAssoc
            maxVar = x

    return maxVar, maxMinAssoc


def FAS(data, network=None, maxNetwork=None):
    # TODO: what to do with network and maxNetwork?

    # Start with a fully connected graph
    skeleton = nx.DiGraph()
    skeleton.add_edges_from([(v1, v2) for v1 in data.variables
                            for v2 in data.variables if v1 != v2])

    # Initially assume all variables are dependent
    edges = set(skeleton.edges_iter())

    n = 0   # Size of candidate conditioning sets

    while len(edges) > 0:
        for x, y in edges:
            adjacent = set(skeleton.successors_iter(x)) - {y}

            if len(adjacent) < n:
                edges = edges - {(x, y)}

            for condSet in itertools.combinations(adjacent, n):
                if stats.independent(data, x, y, condSet):
                    skeleton.remove_edge(x, y)
                    edges = edges - {(x, y)}
                    break

        n += 1

    network = bn.BayesNet()
    for edge in skeleton.edges_iter():
        network.addEdge(*edge)

    return network


def learnDBN(dataSlice0, dataSlice1, alg=MMHC):
    assert(len(dataSlice0) == len(dataSlice1))
    assert(dataSlice0.variables == dataSlice1.variables)

    # Learn the initial network of the DBN
    initial = alg(dataSlice0)
    estimateParams(initial, dataSlice0)

    # Combine data slices for learning the transition network
    slice0Vars = dataSlice0.variables
    slice1Vars = tuple(v + '_' for v in dataSlice1.variables)
    timeData = dataSlice0.addColumns(slice1Vars, dataSlice1.data)

    # Set constraints on structure of transition network
    edges = itertools.chain(itertools.product(slice0Vars, slice1Vars),
                            itertools.product(slice1Vars, slice1Vars))
    maxNetwork = bn.BayesNet()
    maxNetwork.addEdges(edges)

    # Learn the transition network
    transition = alg(timeData, maxNetwork=maxNetwork)
    estimateParams(transition, timeData)

    return bn.DynamicBayesNet(initial, transition)
