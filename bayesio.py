import bayesnet as bn
import cpd


def parseBIF(filename):
    '''
    Parse a .bif file and get the encoded Bayesian network.

    filename : The name of the .bif file to be parsed.
    '''

    # TODO: Add support for properties, comments, and non-discrete variables

    tokens = ('network', 'variable', 'probability', 'type', 'table', '{', '}',
              '(', ')', '[', ']', ',', ';', '|')
    whitespace = (' ', '\t', '\n', '\r')

    def parseNetwork(s, i):
        attr = {}
        attr['name'], i = nextToken(s, i)
        token, i = nextToken(s, i)
        assert(token == '{')

        curlyCount = 1
        while True:
            token, i = nextToken(s, i)

            if token == '{':
                curlyCount += 1
            elif token == '}':
                curlyCount -= 1

            if curlyCount == 0:
                return attr, i

    def parseVariable(s, i):
        attr = {}
        attr['name'], i = nextToken(s, i)
        token, i = nextToken(s, i)
        assert(token == '{')
        token, i = nextToken(s, i)
        assert(token == 'type')
        token, i = nextToken(s, i)
        assert(token == 'discrete')
        token, i = nextToken(s, i)
        assert(token == '[')
        token, i = nextToken(s, i)
        int(token)
        token, i = nextToken(s, i)
        assert(token == ']')
        token, i = nextToken(s, i)
        assert(token == '{')

        domain = ()
        while True:
            val, i = nextToken(s, i)
            domain += (val,)

            token, i = nextToken(s, i)
            assert(token == ',' or token == '}')

            if token == '}':
                break
        attr['domain'] = domain

        token, i = nextToken(s, i)
        assert(token == ';')
        token, i = nextToken(s, i)
        assert(token == '}')

        return attr, i

    def parseProbability(s, i, domains):
        condVars = ()
        token, i = nextToken(s, i)
        assert(token == '(')

        varName, i = nextToken(s, i)

        token, i = nextToken(s, i)
        assert(token == '|' or token == ')')

        if token == '|':
            # Parse names of variables to condition on
            while True:
                condVar, i = nextToken(s, i)
                condVars += (condVar,)
                token, i = nextToken(s, i)
                assert(token == ',' or token == ')')

                if token == ')':
                    break
        else:
            condVars = ()

        # Create an empty CPD
        cpd = CPD.TableCPD(varName, condVars, {})

        token, i = nextToken(s, i)
        assert(token == '{')

        token, i = nextToken(s, i)
        if token == 'table':
            probs, i = parseNumberList(s, i)

            # Poplulate the CPD with probabilities
            for val, p in zip(domains[varName], probs):
                cpd.setProb({varName: val}, float(p))

            token, i = nextToken(s, i)
        else:
            while token == '(':
                vals, i = parseValueList(s, i - 1)
                probs, i = parseNumberList(s, i)

                # Poplulate the CPD with probabilities
                for val, p in zip(domains[varName], probs):
                    inst = {condVar: val for condVar, val in zip(condVars, vals)}
                    inst.update({varName: val})
                    cpd.setProb(inst, float(p))

                token, i = nextToken(s, i)

        assert(token == '}')
        return cpd, i

    def parseNumberList(s, i):
        numbers = ()
        while True:
            number, i = nextToken(s, i)
            numbers += (number,)

            token, i = nextToken(s, i)
            assert(token == ',' or token == ';')

            if token == ';':
                break

        return numbers, i

    def parseValueList(s, i):
        values = ()
        token, i = nextToken(s, i)
        assert(token == '(')

        while True:
            value, i = nextToken(s, i)
            values += (value,)

            token, i = nextToken(s, i)
            assert(token == ',' or token == ')')

            if token == ')':
                break

        return values, i

    def parseNumber(s, i):
        j = i
        while True:
            if s[j] in '0123456789.':
                j += 1
            else:
                return s[i:j], j

    def parseName(s, i):
        j = i
        while True:
            if s[j].isalnum():
                j += 1
            else:
                return s[i:j], j

    def nextToken(s, i):
        if i >= len(s):
            return None, i

        if s[i] in whitespace:
            ret = nextToken(s, i + 1)
            #print(ret)
            return ret

        if s[i].isdecimal():
            ret = parseNumber(s, i)
            #print(ret)
            return ret

        for token in tokens:
            if s[i:].startswith(token):
                ret = token, i + len(token)
                #print(ret)
                return ret

        if s[i].isalnum():
            ret = parseName(s, i)
            #print(ret)
            return ret

        raise Exception('Unknown token')

    contents = ''
    domains = {}
    network = bn.BayesNet()

    with open(filename, 'r') as f:
        contents = f.read()

    i = 0
    while True:
        token, i = nextToken(contents, i)

        if token == 'network':
            attr, i = parseNetwork(contents, i)
            network.updateAttributes(attr)

        elif token == 'variable':
            attr, i = parseVariable(contents, i)
            network.addNode(attr['name'])
            domains[attr['name']] = attr['domain']

        elif token == 'probability':
            cpd, i = parseProbability(contents, i, domains)
            network.updateCPD(cpd.child, cpd)

            for parent in cpd.parents:
                network.addEdge(parent, cpd.child)

        elif token is None:
            break

    return network
