class Factor():
    def __init__(self):
        self.table = {}
        self.varNames = set()

    def __len__(self):
        return len(self.table)

    def getValue(self, inst):
        '''
        Look up a value from the factor.

        inst : An instantiation of variables (i.e. a mapping from variable
            names to values).
        '''

        key = frozenset(inst.items())
        return self.table[key]

    def getValues(self):
        '''
        Get the values contained in the factor. No guarantees are made about
        the order of values in the sequence.
        '''

        return tuple(self.table.values())

    def setRow(self, inst, value):
        '''
        Set a value for an instantiation of variables.

        inst : An instantiation of variables (i.e. a mapping from variable
            names to values).
        '''

        # Ensure each row contains the same set of variables
        varNames = set(inst.keys())
        if len(self.table) > 0 and varNames != self.varNames:
            raise ValueError('All rows in a Factor object must contain the '
                             'same set of variables')
        self.varNames = varNames

        key = frozenset(inst.items())
        self.table[key] = value

    def scope(self):
        '''
        Get the scope of the factor.
        '''

        return self.varNames

    def marginalize(self, varName):
        '''
        Sum over a particular variable from the factor.

        varName : The variable to sum over.
        '''

        #TODO: ensure that summing over variables not in scope works correctly

        result = Factor()
        result.varNames = self.varNames - {varName}

        for inst, val in self.table.items():
            reducedInst = frozenset({assignment for assignment in inst if
                                     assignment[0] != varName})

            if reducedInst in result.table:
                result.table[reducedInst] = result.table[reducedInst] + val
            else:
                result.table[reducedInst] = val

        return result

    def reduce(self, inst):
        '''
        Reduce the factor given a set of observed variables.

        inst : An instantiation of variables (i.e. a mapping from variable
            names to values).
        '''

        # Get rid of irrelevant observations
        observed = {key: val for key, val in inst.items() if key in
                    self.varNames}
        reducedInst = frozenset(observed.items())

        result = Factor()
        result.varNames = self.varNames - set(inst.keys())

        for currentInst, val in self.table.items():
            if reducedInst <= currentInst:
                newInst = currentInst - reducedInst
                result.table[newInst] = val

        return result

    def normalize(self):
        '''
        Normalize the factor so that all entries sum to 1.
        '''

        total = sum(self.table.values())

        result = Factor()
        result.varNames = self.varNames
        result.table = {key: (val / total) for key, val in self.table.items()}

        return result

    def __mul__(self, other):
        '''
        Multiply this factor by another factor.

        other : The factor to multiply with.
        '''

        if not isinstance(other, Factor):
            raise TypeError('Cannot multiply a Factor type with a non-Factor '
                            'type')

        result = Factor()
        result.varNames = self.varNames | other.varNames

        # Determine which variables the factors have in common
        sharedVars = self.varNames & other.varNames

        # Multiply each row in this factor by each row in the other factor
        # that has the same instantiation for the shared variables
        for inst, val in self.table.items():
            sharedInst = frozenset({assignment for assignment in inst if
                                    assignment[0] in sharedVars})
            multiplied = {otherInst: otherVal for otherInst, otherVal in
                          other.table.items() if sharedInst <= otherInst}

            for otherInst, otherVal in multiplied.items():
                newInst = frozenset(inst | otherInst)
                result.table[newInst] = val * otherVal

        return result
