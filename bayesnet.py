import copy

import networkx as nx

import util


class BayesNet():
    '''
    A class to represent a Bayesian network.
    '''

    def __init__(self, network=None, **attr):
        '''
        Initialize a Bayesian network.

        network : A BayesNet to copy to this object.
        attr : Attributes in key=value format to add to the network.
        '''

        if network is not None:
            self.graph = nx.DiGraph(network.graph)
            self.cpds = dict(network.cpds)
            self.attributes = dict(network.attributes)
        else:
            self.graph = nx.DiGraph()
            self.cpds = {}
            self.attributes = {}

        self.attributes.update(attr)

    def __len__(self):
        '''
        Get the number of nodes in the network.
        '''

        return len(self.graph)

    def __getitem__(self, varName):
        '''
        Get the conditional probability distribution corresponding to a
        variable.
        '''

        return self.cpds[varName]

    def __iter__(self):
        '''
        Iterate over the nodes in the network. Each item returned by the
        iterator is a 2-tuple where the first item is the variable name and the
        second item is the corresponding CPD.
        '''

        return iter(self.cpds.items())

    def __contains__(self, varName):
        '''
        Overloads the 'in' operator.
        Returns True if varName is the name of a variable in the network, False
        otherwise.
        '''

        return varName in self.cpds.keys()

    def updateAttributes(self, attr):
        self.attributes.update(attr)

    def addNode(self, varName, cpd=None):
        '''
        Add a node to the network. If the node already exists, the CPD is
        updated.

        varName : A string containing the name of the variable.
        cpd : A conditional probability distribution for the variable.
        '''

        self.graph.add_node(varName)
        self.cpds[varName] = cpd

    def addEdge(self, parent, child):
        '''
        Add a directed edge from parent to child. Cycles are not checked for
        when adding an edge in order to make structure learning convenient. To
        detect cycles in the network, use isDAG().

        '''

        self.graph.add_edge(parent, child)

    def addEdges(self, edges):
        '''
        Add a collection of edges to the network.

        edges : A collection of edges to be added to the network. Each item in
            the collection must be a two element sequence containing the parent
            and child that share the edge.
        '''

        for edge in edges:
            self.addEdge(*edge)

    def removeEdge(self, node1, node2):
        '''
        Remove an edge from the network.

        node1, node2 : The nodes to remove the edge between.
        '''

        try:
            self.graph.remove_edge(node1, node2)
        except nx.NetworkXError:
            raise KeyError

    def reverseEdge(self, node1, node2):
        try:
            self.graph.remove_edge(node1, node2)
            self.graph.add_edge(node2, node1)
        except nx.NetworkXError:
            raise KeyError

    def edges(self):
        return self.graph.edges()

    def edgesIter(self):
        return self.graph.edges_iter()

    def nodes(self):
        return self.graph.nodes()

    def nodesIter(self):
        return self.graph.nodes_iter()

    def topologicalSort(self):
        return nx.topological_sort(self.graph)

    def getParents(self, varName):
        '''
        Get the names of the parents of a variable.
        '''

        try:
            return self.graph.predecessors(varName)
        except nx.NetworkXError:
            raise KeyError

    def getChildren(self, varName):
        '''
        Get the names of the children of a variable.
        '''

        try:
            return self.graph.successors(varName)
        except nx.NetworkXError:
            raise KeyError

    def updateCPD(self, varName, cpd):
        '''
        Update the conditional probability distribution for a node.
        '''

        if varName in self.nodes():
            self.cpds[varName] = cpd

    def getCPD(self, varName):
        '''
        Get the conditional probability distribution corresponding to a
        variable.
        '''

        return self.cpds[varName]

    def isDAG(self):
        return nx.is_directed_acyclic_graph(self.graph)

    def sample(self):
        sample = {}
        sortedNodes = nx.topological_sort(self.graph)

        for node in sortedNodes:
            cpd = self.getCPD(node)

            probs = []
            values = tuple(cpd.getValues(node))
            for val in values:
                inst = {varName: sample[varName] for varName in self.getParents(node)}
                inst.update({node: val})
                probs.append(cpd.prob(inst))

            choice = util.choice(values, probs)
            sample.update({node: choice})

        return sample

    def dReachable(self, source, observed):
        '''
        Get the set of nodes such that there is at least one unblocked path
        between the source and the node given a set of observed variables. The
        set of nodes returned are conditionally dependent with the source node
        given the observed nodes; that is, they are *not* d-separated.

        Adapted from (Koller and Friedman, 2009), section 3.3.3.

        source : The node to start the search from.
        observed : The set of observed nodes (i.e. nodes to condition on).
        '''

        # Constants indicating whence the search came
        FROM_CHILD = 0
        FROM_PARENT = 1

        toVisit = set(observed)  # nodes to be visited
        ancestors = set()        # ancestors of observed nodes

        while len(toVisit) > 0:
            node = toVisit.pop()

            if node not in ancestors:
                toVisit = self.graph.predecessors(node)

            ancestors.add(node)

        toVisit = set()
        toVisit.add((source, FROM_CHILD))
        visited = set()
        reachable = set()

        while len(toVisit) > 0:
            node, direction = toVisit.pop()

            if (node, direction) not in visited:
                if node not in observed:
                    reachable.add(node)
                visited.add((node, direction))

                if direction == FROM_CHILD and node not in observed:
                    for parent in self.graph.predecessors(node):
                        toVisit.add((parent, FROM_CHILD))
                    for child in self.graph.successors(node):
                        toVisit.add((child, FROM_PARENT))

                elif direction == FROM_PARENT:
                    if node not in observed:
                        for child in self.graph.successors(node):
                            toVisit.add((child, FROM_PARENT))

                    if node in ancestors:
                        for parent in self.graph.predecessors(node):
                            toVisit.add((parent, FROM_CHILD))

        return reachable

    def dSeparated(self, a, b, z):
        '''
        Determine if a set of nodes is d-separated from another set of nodes
        given a conditioning set of nodes. Returns True if the two sets of
        nodes are d-separated given the conditioning set, False otherwise.

        a, b : Sets of nodes to test for d-separation.
        z : The set of observed nodes (i.e. nodes to condition on).
        '''

        for source in a:
            reached = self.dReachable(source, z)

            if len(reached & b) > 0:
                return False

        return True

    def variableElimination(self, query, evidence, ordering=None):
        '''
        Perform exact inference using the variable elimination algorithm.

        query : The set of query variables.
        evidence : An instantiation of observed variables (i.e. a mapping from
            variable names to values).
        ordering : An ordering of variables for elimination. If not given, a
            topological sort of the network is used.
        '''

        eliminate = set(self.graph.nodes()) - (set(query) |
                                               set(evidence.keys()))

        if ordering is None:
            sortedNodes = nx.topological_sort(self.graph)
            ordering = [node for node in sortedNodes if node in eliminate]

        # The set of factors is the set of CPDs for each node, each reduced
        # using the evidence
        factors = [cpd.getFactor().reduce(evidence) for cpd in
                   self.cpds.values()]

        # Eliminate variables according to the ordering
        for node in ordering:
            factors = self._sumProdElimVar(factors, node)

        # Multiply remaining factors to obtain result
        result = None
        for factor in factors:
            if result is not None:
                result = result * factor
            else:
                result = factor

        #TODO: always normalize?
        return result.normalize()

    def _sumProdElimVar(self, factors, varName):
        eliminate = []
        keep = []

        # All factors whose scope contains the specified variable will be
        # summed out
        for factor in factors:
            if varName in factor.scope():
                eliminate.append(factor)
            else:
                keep.append(factor)

        # Multiply all factors that will be eliminated
        product = None
        for factor in eliminate:
            if product is not None:
                product = product * factor
            else:
                product = factor

        # Sum out the specified variable
        result = product.marginalize(varName)

        keep.append(result)

        return keep


class DynamicBayesNet():
    def __init__(self, initial, transition):
        '''
        Initialize a dynamic Bayesian network.

        initial : A BayesNet object representing the initial network.
        transition : A BayesNet object representing the transition model, i.e.
            a conditional Bayesian network that represents a distribution over
            variables given the variables from the previous time slice.
        '''

        self._initial = initial
        self._transition = transition

    @property
    def initial(self):
        return self._initial

    @property
    def transition(self):
        return self._transition

    def unroll(self, n):
        '''
        Unroll the network a number of times.

        n : The number of times to unroll the network. The transition network
            is applied to the initial network n times. For each time slice,
            variable names are appended with an underscore followed by the
            slice number, starting with 0.
        '''

        network = BayesNet()

        # Add nodes and CPDs from initial network
        for node in self._initial.nodesIter():
            cpd = copy.copy(self._initial.getCPD(node))
            child, parents = cpd._child, cpd._parents
            cpd._child = child + '_0'
            cpd._parents = type(parents)(parent + '_0' for parent in parents)
            network.addNode(cpd._child, cpd=cpd)

        # Add edges from initial network
        for parent, child in self._initial.edgesIter():
            assert(parent + '_0' in network.nodes())
            assert(child + '_0' in network.nodes())
            network.addEdge(parent + '_0', child + '_0')

        # Add nodes,edges, and CPDs from transition network for each time slice
        for i in range(1, n + 1):
            for node in self._transition.nodesIter():
                if node.endswith('_'):
                    cpd = copy.copy(self._transition.getCPD(node))
                    child, parents = cpd._child, cpd._parents
                    cpd._child = child + str(i)
                    cpd._parents = type(parents)(
                        parent + (str(i) if parent.endswith('_') else
                                  '_' + str(i - 1))
                        for parent in parents)
                    network.addNode(cpd._child, cpd=cpd)

            for parent, child in self._transition.edgesIter():
                parent = (parent + (str(i) if parent.endswith('_') else
                          '_' + str(i - 1)))
                child = child + str(i)
                assert(parent in network.nodes())
                assert(child in network.nodes())
                network.addEdge(parent, child)

        return network
