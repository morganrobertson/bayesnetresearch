import unittest

import bayesnet as bn
import cpd
import factor


class TestBayesNet(unittest.TestCase):
    def setup(self):
        pass

    def test_dSeparated(self):
        '''
        Test case 1
        Simple chain: x->y->z
        '''
        # Initialize test variables
        network = bn.BayesNet()
        network.addEdge('x', 'y')
        network.addEdge('y', 'z')

        # Call dSeparated() and verify results
        self.assertEqual(network.dSeparated({'x'}, {'y'}, {'z'}), False)
        self.assertEqual(network.dSeparated({'x'}, {'y'}, set()), False)
        self.assertEqual(network.dSeparated({'y'}, {'x'}, {'z'}), False)
        self.assertEqual(network.dSeparated({'y'}, {'x'}, set()), False)

        self.assertEqual(network.dSeparated({'x'}, {'z'}, {'y'}), True)
        self.assertEqual(network.dSeparated({'x'}, {'z'}, set()), False)
        self.assertEqual(network.dSeparated({'z'}, {'x'}, {'y'}), True)
        self.assertEqual(network.dSeparated({'z'}, {'x'}, set()), False)

        self.assertEqual(network.dSeparated({'y'}, {'z'}, {'x'}), False)
        self.assertEqual(network.dSeparated({'y'}, {'z'}, set()), False)
        self.assertEqual(network.dSeparated({'z'}, {'y'}, {'x'}), False)
        self.assertEqual(network.dSeparated({'z'}, {'y'}, set()), False)

        '''
        Test case 2
        Common cause: x<-y->z
        '''
        # Initialize test variables
        network = bn.BayesNet()
        network.addEdge('y', 'x')
        network.addEdge('y', 'z')

        # Call dSeparated() and verify results
        self.assertEqual(network.dSeparated({'x'}, {'y'}, {'z'}), False)
        self.assertEqual(network.dSeparated({'x'}, {'y'}, set()), False)
        self.assertEqual(network.dSeparated({'y'}, {'x'}, {'z'}), False)
        self.assertEqual(network.dSeparated({'y'}, {'x'}, set()), False)

        self.assertEqual(network.dSeparated({'x'}, {'z'}, {'y'}), True)
        self.assertEqual(network.dSeparated({'x'}, {'z'}, set()), False)
        self.assertEqual(network.dSeparated({'z'}, {'x'}, {'y'}), True)
        self.assertEqual(network.dSeparated({'z'}, {'x'}, set()), False)

        self.assertEqual(network.dSeparated({'y'}, {'z'}, {'x'}), False)
        self.assertEqual(network.dSeparated({'y'}, {'z'}, set()), False)
        self.assertEqual(network.dSeparated({'z'}, {'y'}, {'x'}), False)
        self.assertEqual(network.dSeparated({'z'}, {'y'}, set()), False)

        '''
        Test case 3
        V-structure: x->y<-z
        '''
        # Initialize test variables
        network = bn.BayesNet()
        network.addEdge('x', 'y')
        network.addEdge('z', 'y')

        # Call dSeparated() and verify results
        self.assertEqual(network.dSeparated({'x'}, {'y'}, {'z'}), False)
        self.assertEqual(network.dSeparated({'x'}, {'y'}, set()), False)
        self.assertEqual(network.dSeparated({'y'}, {'x'}, {'z'}), False)
        self.assertEqual(network.dSeparated({'y'}, {'x'}, set()), False)

        self.assertEqual(network.dSeparated({'x'}, {'z'}, {'y'}), False)
        self.assertEqual(network.dSeparated({'x'}, {'z'}, set()), True)
        self.assertEqual(network.dSeparated({'z'}, {'x'}, {'y'}), False)
        self.assertEqual(network.dSeparated({'z'}, {'x'}, set()), True)

        self.assertEqual(network.dSeparated({'y'}, {'z'}, {'x'}), False)
        self.assertEqual(network.dSeparated({'y'}, {'z'}, set()), False)
        self.assertEqual(network.dSeparated({'z'}, {'y'}, {'x'}), False)
        self.assertEqual(network.dSeparated({'z'}, {'y'}, set()), False)

        '''
        Test case 4
        Nontrivial network
        '''
        # Initialize test variables
        network = bn.BayesNet()
        network.addEdge('C', 'D')
        network.addEdge('D', 'G')
        network.addEdge('I', 'G')
        network.addEdge('I', 'S')
        network.addEdge('S', 'J')
        network.addEdge('G', 'H')
        network.addEdge('G', 'L')
        network.addEdge('L', 'J')
        network.addEdge('J', 'H')

        # Call dSeparated() and verify results
        self.assertEqual(network.dSeparated({'C', 'D'}, {'L', 'J', 'S', 'H'},
                         {'G'}), False)
        self.assertEqual(network.dSeparated({'C', 'D'}, {'L', 'J', 'S', 'H'},
                         {'G', 'I'}), True)
        self.assertEqual(network.dSeparated({'C', 'D'}, {'L', 'J', 'H'},
                         {'S'}), False)
        self.assertEqual(network.dSeparated({'C', 'D'}, {'L', 'J', 'H'},
                         {'G', 'S'}), True)
        self.assertEqual(network.dSeparated({'L', 'J', 'S', 'H'}, {'C', 'D'},
                         {'G'}), False)
        self.assertEqual(network.dSeparated({'L', 'J', 'S', 'H'}, {'C', 'D'},
                         {'G', 'I'}), True)
        self.assertEqual(network.dSeparated({'L', 'J', 'S', 'H'}, {'C', 'D'},
                         {'S'}), False)
        self.assertEqual(network.dSeparated({'L', 'J', 'S', 'H'}, {'C', 'D'},
                         {'G', 'S'}), True)

    def test_variableElimination(self):
        '''
        Test case 1
        Simple chain: x->y
        '''
        # Initialize CPD for each variable
        f_x = factor.Factor()
        f_y = factor.Factor()

        f_x.setRow({'x': 0}, 0.8)
        f_x.setRow({'x': 1}, 0.2)

        f_y.setRow({'x': 0, 'y': 0}, 0.7)
        f_y.setRow({'x': 0, 'y': 1}, 0.3)
        f_y.setRow({'x': 1, 'y': 0}, 0.6)
        f_y.setRow({'x': 1, 'y': 1}, 0.4)

        cpd_x = cpd.TableCPD('x', set(), f_x)
        cpd_y = cpd.TableCPD('y', {'x'}, f_y)

        # Create the network
        network = bn.BayesNet()
        network.addNode('x', cpd_x)
        network.addNode('y', cpd_y)
        network.addEdge('x', 'y')

        # Call variableElimination() and verify the results

        # P(y) given x as evidence
        result_y = network.variableElimination('y', {'x': 0})
        self.assertAlmostEqual(result_y.getValue({'y': 0}), 0.7)
        self.assertAlmostEqual(result_y.getValue({'y': 1}), 0.3)

        result_y = network.variableElimination('y', {'x': 1})
        self.assertAlmostEqual(result_y.getValue({'y': 0}), 0.6)
        self.assertAlmostEqual(result_y.getValue({'y': 1}), 0.4)

        # P(x) given y as evidence
        result_x = network.variableElimination('x', {'y': 0})
        self.assertAlmostEqual(result_x.getValue({'x': 0}), 14 / 17)
        self.assertAlmostEqual(result_x.getValue({'x': 1}), 3 / 17)

        result_x = network.variableElimination('x', {'y': 1})
        self.assertAlmostEqual(result_x.getValue({'x': 0}), 0.75)
        self.assertAlmostEqual(result_x.getValue({'x': 1}), 0.25)

        # P(y) given no evidence
        result_y = network.variableElimination('y', {})
        self.assertAlmostEqual(result_y.getValue({'y': 0}), 0.68)
        self.assertAlmostEqual(result_y.getValue({'y': 1}), 0.32)

        # P(x) given no evidence
        result_x = network.variableElimination('x', {})
        self.assertAlmostEqual(result_x.getValue({'x': 0}), 0.8)
        self.assertAlmostEqual(result_x.getValue({'x': 1}), 0.2)

if __name__ == '__main__':
    unittest.main()
