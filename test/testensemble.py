import unittest

import numpy as np

import cstats
import database
import ensemble


class TestEnsemble(unittest.TestCase):
    def test_LGNEnsemble_init(self):
        mean1 = np.array([0, 0])
        mean2 = np.array([5, 5])
        mean3 = np.array([-3, 2])
        cov1 = np.random.rand(2, 2)
        cov1 = cov1 * cov1.T
        cov2 = np.random.rand(2, 2)
        cov2 = cov2 * cov2.T
        cov3 = np.random.rand(2, 2)
        cov3 = cov3 * cov3.T

        data1 = np.random.multivariate_normal(mean1, cov1, 25)
        data2 = np.random.multivariate_normal(mean2, cov2, 50)
        data3 = np.random.multivariate_normal(mean3, cov3, 100)
        data = np.concatenate((data1, data2, data3))
        db = database.TableDB(('x', 'y'), data, datatype=database.CONTINUOUS)

        nNetworks = 10
        k = 3
        nAttr = 2

        # Create an ensemble of LGNs
        e = ensemble.LGNEnsemble(db, nNetworks=nNetworks, k=k, nAttr=nAttr)

        self.assertEqual(len(e), nNetworks)

        for lgn in e:
            self.assertEqual(len(lgn), nAttr)

        '''
        print('mean1:\t{}'.format(mean1))
        print('mean2:\t{}'.format(mean2))
        print('mean2:\t{}'.format(mean3))
        print()

        for network in e:
            g = cstats.Gaussian(network=network)
            print('{}\t{}'.format(g.variables, np.round(g.mean)))
        '''

    def test_LGNEnsemble_toGMM(self):
        e = self.makeLGNEnsemble()

        gmm = e.toGMM()

        self.assertEqual(len(e), len(gmm))

    def test_LGNEnsemble_inference(self):
        e = self.makeLGNEnsemble()

        y1 = e.inference({'y'}, {'x': 0})
        y2 = e.inference({'y'}, {'x': 5})
        y3 = e.inference({'y'}, {'x': -3})

        print('y1:\t{}'.format(y1))
        print('y2:\t{}'.format(y2))
        print('y3:\t{}'.format(y3))

    def makeLGNEnsemble(self):
        mean1 = np.array([0, 0])
        mean2 = np.array([5, 5])
        mean3 = np.array([-3, 2])
        cov1 = np.random.rand(2, 2)
        cov1 = cov1 * cov1.T
        cov2 = np.random.rand(2, 2)
        cov2 = cov2 * cov2.T
        cov3 = np.random.rand(2, 2)
        cov3 = cov3 * cov3.T

        data1 = np.random.multivariate_normal(mean1, cov1, 25)
        data2 = np.random.multivariate_normal(mean2, cov2, 50)
        data3 = np.random.multivariate_normal(mean3, cov3, 100)
        data = np.concatenate((data1, data2, data3))
        db = database.TableDB(('x', 'y'), data, datatype=database.CONTINUOUS)

        nNetworks = 10
        k = 3
        nAttr = 2

        return ensemble.LGNEnsemble(db, nNetworks, k, nAttr)


if __name__ == '__main__':
    unittest.main()
