import unittest

import numpy as np

import cstats
import database
import learning
import util_test


class TestCstats(unittest.TestCase):

    def test_partialCorrelation(self):
        print('Testing partialCorrelation()')
        varNames = ('a', 'b', 'c', 'd')

        # Create a dataset with linear correlations and Gaussian noise
        np.random.seed(0)
        mean = np.zeros(4)
        cov = np.random.randn(4, 4)
        cov = cov * cov.T
        data = np.random.multivariate_normal(mean, cov, 1000)
        db = database.TableDB(varNames, data, database.CONTINUOUS)

        # Compute precision matrix
        w = np.linalg.inv(db.covariance())

        # Compute partial correlation using all other variables as control set
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'a', 'b'),
                               (-w[0, 1] / np.sqrt(w[0, 0] * w[1, 1])))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'a', 'c'),
                               (-w[0, 2] / np.sqrt(w[0, 0] * w[2, 2])))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'a', 'd'),
                               (-w[0, 3] / np.sqrt(w[0, 0] * w[3, 3])))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'b', 'c'),
                               (-w[1, 2] / np.sqrt(w[1, 1] * w[2, 2])))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'b', 'd'),
                               (-w[1, 3] / np.sqrt(w[1, 1] * w[3, 3])))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'c', 'd'),
                               (-w[2, 3] / np.sqrt(w[2, 2] * w[3, 3])))

        # Verify that partial correlation given an empty set of control
        # variables is equal to regular correlation
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'a', 'b', set()),
                               cstats.correlation(db, 'a', 'b'))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'a', 'c', set()),
                               cstats.correlation(db, 'a', 'c'))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'a', 'd', set()),
                               cstats.correlation(db, 'a', 'd'))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'b', 'c', set()),
                               cstats.correlation(db, 'b', 'c'))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'b', 'd', set()),
                               cstats.correlation(db, 'b', 'd'))
        self.assertAlmostEqual(cstats.partialCorrelation(db, 'c', 'd', set()),
                               cstats.correlation(db, 'c', 'd'))

    def test_gaussianPdf(self):
        x = np.zeros(4)
        mean = np.zeros(4)
        cov = np.eye(4)

        result = cstats.gaussianPdf(x, mean, cov)

    def test_networkToGaussian(self):
        # Build linear Gaussian network
        network, db, meta = util_test.readLGN('0')
        learning.gaussianMLE(network, db)
        nNodes = len(network)

        # Call networkToGaussian()
        mean, cov, varNames = cstats.networkToGaussian(network)

        self.assertEqual(len(mean), nNodes)
        self.assertEqual(cov.shape, (nNodes, nNodes))
        self.assertEqual(set(varNames), set(network.nodes()))

    def test_Gaussian_partition(self):
        cov = np.random.rand(4, 4)
        cov = cov * cov.T
        mean = np.random.random_integers(10, size=4)
        g = cstats.Gaussian(mean, cov, ('a', 'b', 'c', 'd'))

        g1 = g.partition([])
        g2 = g.partition(('a',))
        g3 = g.partition(('b', 'd'))
        g4 = g.partition(('a', 'b', 'c', 'd'))

        self.assertEqual(tuple(g1.variables), tuple())
        self.assertEqual(len(g1.mean), 0)
        self.assertEqual(g1.covariance.shape, (0, 0))

        self.assertEqual(tuple(g2.variables), ('a',))
        self.assertEqual(len(g2.mean), 1)
        self.assertEqual(g2.covariance.shape, (1, 1))

        self.assertEqual(tuple(g3.variables), ('b', 'd'))
        self.assertEqual(len(g3.mean), 2)
        self.assertEqual(g3.covariance.shape, (2, 2))

        self.assertEqual(tuple(g4.variables), ('a', 'b', 'c', 'd'))
        self.assertEqual(len(g4.mean), 4)
        self.assertEqual(g4.covariance.shape, (4, 4))

    def test_Gaussian_marginalize(self):
        cov = np.random.rand(4, 4)
        cov = cov * cov.T
        mean = np.random.random_integers(10, size=4)
        g = cstats.Gaussian(mean, cov, ('a', 'b', 'c', 'd'))

        g1 = g.marginalize([])
        g2 = g.marginalize(('a'))
        g3 = g.marginalize(('b', 'd'))
        g4 = g.marginalize(('a', 'b', 'c', 'd'))

        self.assertEqual(set(g1.variables), {'a', 'b', 'c', 'd'})
        self.assertEqual(len(g1.mean), 4)
        self.assertEqual(g1.covariance.shape, (4, 4))

        self.assertEqual(set(g2.variables), {'b', 'c', 'd'})
        self.assertEqual(len(g2.mean), 3)
        self.assertEqual(g2.covariance.shape, (3, 3))

        self.assertEqual(set(g3.variables), {'a', 'c'})
        self.assertEqual(len(g3.mean), 2)
        self.assertEqual(g3.covariance.shape, (2, 2))

        self.assertEqual(set(g4.variables), set())
        self.assertEqual(len(g4.mean), 0)
        self.assertEqual(g4.covariance.shape, (0, 0))

    def test_Gaussian_condition(self):
        cov = np.random.rand(4, 4)
        cov = cov * cov.T
        mean = np.random.random_integers(10, size=4)
        g = cstats.Gaussian(mean, cov, ('a', 'b', 'c', 'd'))

        g1 = g.condition({})
        g2 = g.condition({'a': 1.0})
        g3 = g.condition({'b': 2.5, 'd': 3.8})
        g4 = g.condition({'a': 1.0, 'b': 2.5, 'c': 2.7, 'd': 3.8})

        self.assertEqual(set(g1.variables), {'a', 'b', 'c', 'd'})
        self.assertEqual(len(g1.mean), 4)
        self.assertEqual(g1.covariance.shape, (4, 4))

        self.assertEqual(set(g2.variables), {'b', 'c', 'd'})
        self.assertEqual(len(g2.mean), 3)
        self.assertEqual(g2.covariance.shape, (3, 3))

        self.assertEqual(set(g3.variables), {'a', 'c'})
        self.assertEqual(len(g3.mean), 2)
        self.assertEqual(g3.covariance.shape, (2, 2))

        self.assertEqual(set(g4.variables), set())
        self.assertEqual(len(g4.mean), 0)
        self.assertEqual(g4.covariance.shape, (0, 0))

    def test_Gaussian_inference(self):
        cov = np.random.rand(4, 4)
        cov = cov * cov.T
        mean = np.random.random_integers(10, size=4)
        g = cstats.Gaussian(mean, cov, ('a', 'b', 'c', 'd'))

        g1 = g.inference(('a', 'b', 'c', 'd'), {})
        g2 = g.inference(('b', 'c', 'd'), {'a': 1.0})
        g3 = g.inference(('a',), {'b': 2.5, 'd': 3.8})

        self.assertEqual(set(g1.variables), {'a', 'b', 'c', 'd'})
        self.assertEqual(len(g1.mean), 4)
        self.assertEqual(g1.covariance.shape, (4, 4))

        self.assertEqual(set(g2.variables), {'b', 'c', 'd'})
        self.assertEqual(len(g2.mean), 3)
        self.assertEqual(g2.covariance.shape, (3, 3))

        self.assertEqual(set(g3.variables), {'a'})
        self.assertEqual(len(g3.mean), 1)
        self.assertEqual(g3.covariance.shape, (1, 1))

if __name__ == '__main__':
    unittest.main()
