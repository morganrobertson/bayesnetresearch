import unittest
from collections import defaultdict

import scipy.misc

import util


class TestUtil(unittest.TestCase):

    def test_subsets(self):
        items = {'a', 'b', 'c', 'd'}
        counts = defaultdict(int)

        # Verify that each returned subset is indeed a subset
        for subset in util.subsets(items):
            self.assertTrue(subset <= items)
            counts[len(subset)] += 1

        # Verify that the proper number of subsets of each length was returned
        for size, count in counts.items():
            expectedCount = scipy.misc.comb(len(items), size, exact=True)
            self.assertEqual(count, expectedCount)

if __name__ == '__main__':
    unittest.main()
