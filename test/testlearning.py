import os
import sys
import unittest

import numpy as np

import bayesnet as bn
import cpd
import database
import learning
import util_test


class TestLearning(unittest.TestCase):

    def test_gaussianMLE(self):
        network, db, meta = util_test.readLGN('0')

        # Call gaussianMLE()
        learning.gaussianMLE(network, db)

    def test_gaussianEM(self):
        data1 = np.random.multivariate_normal(np.zeros(2), np.eye(2), 10)
        data2 = np.random.multivariate_normal(3 * np.ones(2), np.eye(2), 10)
        data = np.concatenate((data1, data2))
        db = database.TableDB(('x', 'y'), data, datatype=database.CONTINUOUS)

        learning.gaussianEM(db, 2, iterations=10)

    def test_calculateBGe(self):
        network, db, meta = util_test.readLGN('0')

        # Add a CPD to each node
        for node in network.nodes():
            c = cpd.LinearGaussianCPD(node, network.getParents(node), 0, [], 0)
            network.updateCPD(node, c)

        # Call calculateBGe()
        score = learning.calculateBGe(network, db)

    def test_hillClimbing(self):
        print('\ntest_hillClimbing()')

        network, db, meta = util_test.readLGN('00')

        # Call hillClimbing()
        skeleton = learning.hillClimbing(db)

        tp = len(set(network.edges()) & set(skeleton.edges()))
        fn = len(set(network.edges()) - set(skeleton.edges()))
        fp = len(set(skeleton.edges()) - set(network.edges()))

        print('True structure:\t{}'.format(sorted(network.edges())))
        print('Learned structure:\t{}'.format(sorted(skeleton.edges())))
        print('True positives:\t{}'.format(tp))
        print('False negatives:\t{}'.format(fn))
        print('False positives:\t{}'.format(fp))

    def test_MMHC(self):
        print('\ntest_MMHC()')

        network, db, meta = util_test.readLGN('00')

        # Call MMHC()
        skeleton = learning.MMHC(db)

        tp = len(set(network.edges()) & set(skeleton.edges()))
        fn = len(set(network.edges()) - set(skeleton.edges()))
        fp = len(set(skeleton.edges()) - set(network.edges()))

        print('True structure:\t{}'.format(sorted(network.edges())))
        print('Learned structure:\t{}'.format(sorted(skeleton.edges())))
        print('True positives:\t{}'.format(tp))
        print('False negatives:\t{}'.format(fn))
        print('False positives:\t{}'.format(fp))

    def test_FAS(self):
        print('\ntest_FAS()')

        network, db, meta = util_test.readLGN('00')

        # Call FAS()
        skeleton = learning.FAS(db)

        tp = len(set(network.edges()) & set(skeleton.edges()))
        fn = len(set(network.edges()) - set(skeleton.edges()))
        fp = len(set(skeleton.edges()) - set(network.edges()))

        print('True structure:\t{}'.format(sorted(network.edges())))
        print('Learned structure:\t{}'.format(sorted(skeleton.edges())))
        print('True positives:\t{}'.format(tp))
        print('False negatives:\t{}'.format(fn))
        print('False positives:\t{}'.format(fp))

if __name__ == '__main__':
    unittest.main()
