import unittest

import cpd
import factor


class TestCPD(unittest.TestCase):

    def test_prob(self):
        f = factor.Factor()

        f.setRow({'a': 0, 'b': 0}, 0.7)
        f.setRow({'a': 0, 'b': 1}, 0.3)
        f.setRow({'a': 1, 'b': 0}, 0.6)
        f.setRow({'a': 1, 'b': 1}, 0.4)

        c = cpd.TableCPD('b', {'a'}, f)

        self.assertEqual(c.prob({'a': 0, 'b': 0}), 0.7)
        self.assertEqual(c.prob({'a': 0, 'b': 1}), 0.3)
        self.assertEqual(c.prob({'a': 1, 'b': 0}), 0.6)
        self.assertEqual(c.prob({'a': 1, 'b': 1}), 0.4)

    def test_getValues(self):
        f = factor.Factor()

        f.setRow({'a': 0, 'b': 0}, 0.7)
        f.setRow({'a': 0, 'b': 1}, 0.3)
        f.setRow({'a': 1, 'b': 0}, 0.6)
        f.setRow({'a': 1, 'b': 1}, 0.4)
        f.setRow({'a': 2, 'b': 0}, 0.5)
        f.setRow({'a': 2, 'b': 1}, 0.5)

        c = cpd.TableCPD('b', {'a'}, f)

        self.assertEqual(c.getValues('a'), {0, 1, 2})
        self.assertEqual(c.getValues('b'), {0, 1})

if __name__ == '__main__':
    unittest.main()
