import unittest

import factor


class TestFactor(unittest.TestCase):

    def test_setRow_getValue(self):
        f = factor.Factor()

        inst0 = {'a': 0, 'b': 0}
        inst1 = {'a': 0, 'b': 1}
        inst2 = {'a': 1, 'b': 0}
        inst3 = {'a': 1, 'b': 1}

        f.setRow(inst0, 0)
        f.setRow(inst1, 1)
        f.setRow(inst2, 2)
        f.setRow(inst3, 3)

        self.assertEqual(f.getValue(inst0), 0)
        self.assertEqual(f.getValue(inst1), 1)
        self.assertEqual(f.getValue(inst2), 2)
        self.assertEqual(f.getValue(inst3), 3)

    def test_setRow_scope(self):
        f = factor.Factor()

        inst0 = {'a': 0, 'b': 0}
        inst1 = {'a': 0, 'b': 1}
        inst2 = {'a': 1, 'b': 0}
        inst3 = {'a': 1, 'b': 1}

        f.setRow(inst0, 0)
        f.setRow(inst1, 1)
        f.setRow(inst2, 2)
        f.setRow(inst3, 3)

        varNames = set(inst0.keys())

        self.assertEqual(f.scope(), varNames)

    def test_marginalize(self):
        f_abc = factor.Factor()

        f_abc.setRow({'a': 0, 'b': 0, 'c': 0}, 0)
        f_abc.setRow({'a': 0, 'b': 0, 'c': 1}, 1)
        f_abc.setRow({'a': 0, 'b': 1, 'c': 0}, 2)
        f_abc.setRow({'a': 0, 'b': 1, 'c': 1}, 3)
        f_abc.setRow({'a': 1, 'b': 0, 'c': 0}, 4)
        f_abc.setRow({'a': 1, 'b': 0, 'c': 1}, 5)
        f_abc.setRow({'a': 1, 'b': 1, 'c': 0}, 6)
        f_abc.setRow({'a': 1, 'b': 1, 'c': 1}, 7)

        f_bc = f_abc.marginalize('a')
        f_ac = f_abc.marginalize('b')
        f_ab = f_abc.marginalize('c')

        self.assertEqual(f_bc.scope(), {'b', 'c'})
        self.assertEqual(f_ac.scope(), {'a', 'c'})
        self.assertEqual(f_ab.scope(), {'a', 'b'})

        self.assertEqual(f_bc.getValue({'b': 0, 'c': 0}), 4)
        self.assertEqual(f_bc.getValue({'b': 0, 'c': 1}), 6)
        self.assertEqual(f_bc.getValue({'b': 1, 'c': 0}), 8)
        self.assertEqual(f_bc.getValue({'b': 1, 'c': 1}), 10)

        self.assertEqual(f_ac.getValue({'a': 0, 'c': 0}), 2)
        self.assertEqual(f_ac.getValue({'a': 0, 'c': 1}), 4)
        self.assertEqual(f_ac.getValue({'a': 1, 'c': 0}), 10)
        self.assertEqual(f_ac.getValue({'a': 1, 'c': 1}), 12)

        self.assertEqual(f_ab.getValue({'a': 0, 'b': 0}), 1)
        self.assertEqual(f_ab.getValue({'a': 0, 'b': 1}), 5)
        self.assertEqual(f_ab.getValue({'a': 1, 'b': 0}), 9)
        self.assertEqual(f_ab.getValue({'a': 1, 'b': 1}), 13)

    def test_reduce(self):
        f = factor.Factor()

        f.setRow({'a': 0, 'b': 0, 'c': 0}, 0)
        f.setRow({'a': 0, 'b': 0, 'c': 1}, 1)
        f.setRow({'a': 0, 'b': 1, 'c': 0}, 2)
        f.setRow({'a': 0, 'b': 1, 'c': 1}, 3)
        f.setRow({'a': 1, 'b': 0, 'c': 0}, 4)
        f.setRow({'a': 1, 'b': 0, 'c': 1}, 5)
        f.setRow({'a': 1, 'b': 1, 'c': 0}, 6)
        f.setRow({'a': 1, 'b': 1, 'c': 1}, 7)
        f.setRow({'a': 2, 'b': 0, 'c': 0}, 8)
        f.setRow({'a': 2, 'b': 0, 'c': 1}, 9)
        f.setRow({'a': 2, 'b': 1, 'c': 0}, 10)
        f.setRow({'a': 2, 'b': 1, 'c': 1}, 11)

        f_a = f.reduce({'a': 0})
        f_b = f.reduce({'b': 0})
        f_c = f.reduce({'c': 0})
        f_ab = f.reduce({'a': 0, 'b': 0})
        f_ac = f.reduce({'a': 0, 'c': 0})
        f_bc = f.reduce({'b': 0, 'c': 0})
        f_abc = f.reduce({'a': 0, 'b': 0, 'c': 0})

        self.assertEqual(f_a.scope(), {'b', 'c'})
        self.assertEqual(f_b.scope(), {'a', 'c'})
        self.assertEqual(f_c.scope(), {'a', 'b'})
        self.assertEqual(f_ab.scope(), {'c'})
        self.assertEqual(f_ac.scope(), {'b'})
        self.assertEqual(f_bc.scope(), {'a'})
        self.assertEqual(f_abc.scope(), set())

        self.assertEqual(len(f_a), 4)
        self.assertEqual(len(f_b), 6)
        self.assertEqual(len(f_c), 6)
        self.assertEqual(len(f_ab), 2)
        self.assertEqual(len(f_ac), 2)
        self.assertEqual(len(f_bc), 3)
        self.assertEqual(len(f_abc), 1)

        # Verify that irrelevant evidence is ignored
        f_az = f.reduce({'a': 0, 'z': 0})
        f_bz = f.reduce({'b': 0, 'z': 0})
        f_cz = f.reduce({'c': 0, 'z': 0})
        f_abz = f.reduce({'a': 0, 'b': 0, 'z': 0})
        f_acz = f.reduce({'a': 0, 'c': 0, 'z': 0})
        f_bcz = f.reduce({'b': 0, 'c': 0, 'z': 0})
        f_abcz = f.reduce({'a': 0, 'b': 0, 'c': 0, 'z': 0})

        self.assertEqual(f_a.scope(), {'b', 'c'})
        self.assertEqual(f_b.scope(), {'a', 'c'})
        self.assertEqual(f_c.scope(), {'a', 'b'})
        self.assertEqual(f_ab.scope(), {'c'})
        self.assertEqual(f_ac.scope(), {'b'})
        self.assertEqual(f_bc.scope(), {'a'})
        self.assertEqual(f_abc.scope(), set())

        self.assertEqual(len(f_az), 4)
        self.assertEqual(len(f_bz), 6)
        self.assertEqual(len(f_cz), 6)
        self.assertEqual(len(f_abz), 2)
        self.assertEqual(len(f_acz), 2)
        self.assertEqual(len(f_bcz), 3)
        self.assertEqual(len(f_abcz), 1)

    def test_normalize(self):
        f = factor.Factor()

        f.setRow({'a': 0, 'b': 0, 'c': 0}, 0)
        f.setRow({'a': 0, 'b': 0, 'c': 1}, 1)
        f.setRow({'a': 0, 'b': 1, 'c': 0}, 2)
        f.setRow({'a': 0, 'b': 1, 'c': 1}, 3)
        f.setRow({'a': 1, 'b': 0, 'c': 0}, 4)
        f.setRow({'a': 1, 'b': 0, 'c': 1}, 5)
        f.setRow({'a': 1, 'b': 1, 'c': 0}, 6)
        f.setRow({'a': 1, 'b': 1, 'c': 1}, 7)

        f_norm = f.normalize()

        self.assertEqual(f_norm.scope(), f.scope())
        self.assertAlmostEqual(sum(f_norm.getValues()), 1.0)

    def test_mul(self):
        '''
        Test case 1
        Complete overlap between factor variables
        '''
        f_1 = factor.Factor()
        f_2 = factor.Factor()

        f_1.setRow({'x': 0}, 4)
        f_1.setRow({'x': 1}, 5)
        f_2.setRow({'x': 0}, 6)
        f_2.setRow({'x': 1}, 7)

        f_result = f_1 * f_2

        self.assertEqual(f_result.scope(), {'x'})

        self.assertEqual(f_result.getValue({'x': 0}), 4 * 6)
        self.assertEqual(f_result.getValue({'x': 1}), 5 * 7)

        '''
        Test case 2
        Overlap between multiple factor variables
        '''
        f_abc = factor.Factor()
        f_bcd = factor.Factor()

        f_abc.setRow({'a': 0, 'b': 0, 'c': 0}, 0)
        f_abc.setRow({'a': 0, 'b': 0, 'c': 1}, 1)
        f_abc.setRow({'a': 0, 'b': 1, 'c': 0}, 2)
        f_abc.setRow({'a': 0, 'b': 1, 'c': 1}, 3)
        f_abc.setRow({'a': 1, 'b': 0, 'c': 0}, 4)
        f_abc.setRow({'a': 1, 'b': 0, 'c': 1}, 5)
        f_abc.setRow({'a': 1, 'b': 1, 'c': 0}, 6)
        f_abc.setRow({'a': 1, 'b': 1, 'c': 1}, 7)
        f_abc.setRow({'a': 2, 'b': 0, 'c': 0}, 8)
        f_abc.setRow({'a': 2, 'b': 0, 'c': 1}, 9)
        f_abc.setRow({'a': 2, 'b': 1, 'c': 0}, 10)
        f_abc.setRow({'a': 2, 'b': 1, 'c': 1}, 11)

        f_bcd.setRow({'b': 0, 'c': 0, 'd': 0}, 0)
        f_bcd.setRow({'b': 0, 'c': 0, 'd': 1}, 1)
        f_bcd.setRow({'b': 0, 'c': 1, 'd': 0}, 2)
        f_bcd.setRow({'b': 0, 'c': 1, 'd': 1}, 3)
        f_bcd.setRow({'b': 1, 'c': 0, 'd': 0}, 4)
        f_bcd.setRow({'b': 1, 'c': 0, 'd': 1}, 5)
        f_bcd.setRow({'b': 1, 'c': 1, 'd': 0}, 6)
        f_bcd.setRow({'b': 1, 'c': 1, 'd': 1}, 7)

        f_abcd = f_abc * f_bcd

        self.assertEqual(f_abcd.scope(), {'a', 'b', 'c', 'd'})

        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 0, 'c': 0, 'd': 0}),
                         0 * 0)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 0, 'c': 0, 'd': 1}),
                         0 * 1)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 0, 'c': 1, 'd': 0}),
                         1 * 2)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 0, 'c': 1, 'd': 1}),
                         1 * 3)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 1, 'c': 0, 'd': 0}),
                         2 * 4)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 1, 'c': 0, 'd': 1}),
                         2 * 5)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 1, 'c': 1, 'd': 0}),
                         3 * 6)
        self.assertEqual(f_abcd.getValue({'a': 0, 'b': 1, 'c': 1, 'd': 1}),
                         3 * 7)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 0, 'c': 0, 'd': 0}),
                         4 * 0)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 0, 'c': 0, 'd': 1}),
                         4 * 1)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 0, 'c': 1, 'd': 0}),
                         5 * 2)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 0, 'c': 1, 'd': 1}),
                         5 * 3)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 1, 'c': 0, 'd': 0}),
                         6 * 4)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 1, 'c': 0, 'd': 1}),
                         6 * 5)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 1, 'c': 1, 'd': 0}),
                         7 * 6)
        self.assertEqual(f_abcd.getValue({'a': 1, 'b': 1, 'c': 1, 'd': 1}),
                         7 * 7)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 0, 'c': 0, 'd': 0}),
                         8 * 0)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 0, 'c': 0, 'd': 1}),
                         8 * 1)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 0, 'c': 1, 'd': 0}),
                         9 * 2)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 0, 'c': 1, 'd': 1}),
                         9 * 3)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 1, 'c': 0, 'd': 0}),
                         10 * 4)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 1, 'c': 0, 'd': 1}),
                         10 * 5)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 1, 'c': 1, 'd': 0}),
                         11 * 6)
        self.assertEqual(f_abcd.getValue({'a': 2, 'b': 1, 'c': 1, 'd': 1}),
                         11 * 7)

        '''
        Test case 3
        One factor is a just a number
        '''
        f_1 = factor.Factor()
        f_2 = factor.Factor()

        f_1.setRow({}, 4)
        f_2.setRow({'x': 0}, 5)
        f_2.setRow({'x': 1}, 6)

        f_result_1 = f_1 * f_2
        f_result_2 = f_2 * f_1

        self.assertEqual(f_result_1.scope(), {'x'})
        self.assertEqual(f_result_2.scope(), {'x'})

        self.assertEqual(f_result_1.getValue({'x': 0}), 4 * 5)
        self.assertEqual(f_result_1.getValue({'x': 1}), 4 * 6)
        self.assertEqual(f_result_2.getValue({'x': 0}), 5 * 4)
        self.assertEqual(f_result_2.getValue({'x': 1}), 6 * 4)

if __name__ == '__main__':
    unittest.main()
