import sys

import numpy as np

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('Usage: {} [numberOfVariables] [maxParents] [instances] '
              '[outputFile]'.format(sys.argv[0]))
        sys.exit()

    nVars = int(sys.argv[1])
    maxParents = int(sys.argv[2])
    instances = int(sys.argv[3])
    filename = sys.argv[4]

    # Build a table of relationships between variables
    rel = np.zeros((nVars, nVars))
    for child in range(nVars):
        if child > 0:
            nParents = np.random.randint(min(child, maxParents))
        else:
            nParents = 0

        possibleParents = list(range(child))
        np.random.shuffle(possibleParents)
        parents = possibleParents[:nParents]

        print('{}:\t{}'.format(child, parents))

        for parent in parents:
            rel[child, parent] = np.random.randint(1, 5)

    print(rel)

    # Create the dataset
    data = []
    for i in range(0, instances):
        vals = []
        for child in range(nVars):
            val = np.random.normal(0, 1)

            for parent, parentRel in enumerate(rel[child]):
                if parentRel != 0:
                    val += parentRel * vals[parent]

            vals.append(val)

        data.append(vals)

    # Write data to file
    with open('datasets/data_{}.cnd'.format(filename), 'w') as f:
        for row in data:
            for val in row:
                f.write('{}\t'.format(val))
            f.write('\n')

    # Write structure to file
    with open('datasets/struct_{}.cnd'.format(filename), 'w') as f:
        for rowIdx, row in enumerate(rel):
            for colIdx, val in enumerate(row):
                if val > 0:
                    f.write('{},{}\n'.format(colIdx, rowIdx))

    # Write metadata to file
    with open('datasets/meta_{}.cnd'.format(filename), 'w') as f:
        f.write('VARS:')
        for var in range(nVars):
            f.write('{}'.format(var))

            if var < (nVars - 1):
                f.write(',')
