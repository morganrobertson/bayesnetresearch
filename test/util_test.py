import os
import sys

import numpy as np

import bayesnet as bn
import database


def readLGN(name):
    scriptDir = os.path.dirname(os.path.realpath(sys.argv[0]))
    structurePath = '{}/datasets/struct_{}.cnd'.format(scriptDir, name)
    metadataPath = '{}/datasets/meta_{}.cnd'.format(scriptDir, name)
    dataPath = '{}/datasets/data_{}.cnd'.format(scriptDir, name)

    network = bn.BayesNet()

    # Read network metadata from file
    meta = {}
    with open(metadataPath, 'r') as f:
        for line in f:
            key, value = line.strip().split(':')
            meta.update({key: value})

    varNames = meta['VARS'].strip().split(',')
    for varName in varNames:
        network.addNode(varName)

    # Read network structure from file
    with open(structurePath, 'r') as f:
        for line in f:
            parent, child = line.strip().split(',')
            network.addEdge(parent, child)

    # Read dataset from file
    data = np.loadtxt(dataPath)
    db = database.TableDB(varNames, data, database.CONTINUOUS)

    return network, db, meta
