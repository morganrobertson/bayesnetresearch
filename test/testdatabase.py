import itertools
import sys
import unittest

import numpy as np

import database
import cstats


class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.vals = {'a': {1, 2, 3}, 'b': {4, 5}, 'c': {6, 7, 8},
                     'xyz': {9, 0}}
        self.varNames = ('a', 'b', 'c', 'xyz')
        self.data = np.array([
            [3, 5, 8, 9],
            [3, 5, 8, 0],
            [1, 4, 7, 0],
            [3, 4, 6, 9],
            [3, 5, 6, 0],
            [1, 4, 6, 9],
            [2, 4, 6, 0],
            [1, 5, 7, 9],
            [3, 5, 7, 9],
            [2, 4, 8, 9],
            [3, 4, 6, 9],
            [1, 4, 7, 9],
            [3, 5, 8, 0],
            [1, 4, 8, 9],
            [3, 4, 8, 9],
            [1, 4, 8, 0],
            [1, 5, 7, 9],
            [1, 4, 8, 0],
            [1, 5, 8, 0],
            [3, 5, 8, 9]]
        )
        self.db = database.TableDB(self.varNames, self.data,
                                   datatype=database.DISCRETE)

    def test_values(self):
        for varName, vals in self.vals.items():
            result = self.db.values(varName)
            self.assertEqual(vals, result)

    def test_variables(self):
        result = self.db.variables
        self.assertEqual(self.varNames, result)

    def test_toDict(self):
        pass

    def test_valuesDict(self):
        pass

    def test_subtable(self):
        data = np.array([[0, 1, 2, 3],
                         [4, 5, 6, 7],
                         [8, 9, 0, 1]])
        db = database.TableDB(('a', 'b', 'c', 'd'), data)

        d_a = db.subtable(('a',))
        d_b = db.subtable(('b',))
        d_c = db.subtable(('c',))
        d_d = db.subtable(('d',))
        d_db = db.subtable(('d', 'b'))
        d_cbad = db.subtable(('c', 'b', 'a', 'd'))

        self.assertEqual(list(d_a[:, 0]), list(data[:, 0]))
        self.assertEqual(list(d_b[:, 0]), list(data[:, 1]))
        self.assertEqual(list(d_c[:, 0]), list(data[:, 2]))
        self.assertEqual(list(d_d[:, 0]), list(data[:, 3]))

        self.assertEqual(list(d_db[:, 0]), list(data[:, 3]))
        self.assertEqual(list(d_db[:, 1]), list(data[:, 1]))

        self.assertEqual(list(d_cbad[:, 0]), list(data[:, 2]))
        self.assertEqual(list(d_cbad[:, 1]), list(data[:, 1]))
        self.assertEqual(list(d_cbad[:, 2]), list(data[:, 0]))
        self.assertEqual(list(d_cbad[:, 3]), list(data[:, 3]))

    def test_size(self):
        result = self.db.size()
        size = self.data.shape
        self.assertEqual(size, result)

    def test_count(self):
        # Count how many times each possible instantiation occurs in the
        # dataset. The total count should equal the number of instances in the
        # dataset.

        count = 0
        rows, cols = self.data.shape
        product = itertools.product(*self.vals.values())

        for combo in product:
            inst = {varName: val for varName, val in
                    zip(self.vals.keys(), combo)}
            count += self.db.count(inst)

        self.assertEqual(count, rows)

    def test_simpleContingencyTable(self):
        # TODO
        self.db.simpleContingencyTable('a', 'b', {'c': 6, 'xyz': 9})

    def test_instantiations(self):
        result = self.db.instantiations(self.varNames)
        trueLength = 36
        self.assertEqual(trueLength, len(result))

    def test_mean(self):
        db = database.TableDB(self.db.variables, self.db.data,
                              database.CONTINUOUS)

        mean0 = db.mean()
        mean1 = db.mean(('a', 'b', 'c', 'xyz'))
        mean2 = db.mean(('b', 'c'))

        self.assertEqual(len(mean0), len(db.variables))
        self.assertEqual(len(mean1), 4)
        self.assertEqual(len(mean2), 2)

    def test_covariance(self):
        db = database.TableDB(self.db.variables, self.db.data,
                              database.CONTINUOUS)

        cov0 = db.covariance()
        cov1 = db.covariance(('a', 'b', 'c', 'xyz'))
        cov2 = db.covariance(('b', 'c'))

        self.assertEqual(cov0.shape, (len(db.variables), len(db.variables)))
        self.assertEqual(cov1.shape, (4, 4))
        self.assertEqual(cov2.shape, (2, 2))

    def test_bag(self):
        # Tests for normal bagging
        bag0 = self.db.bag()

        self.assertEqual(len(bag0), len(self.db))
        self.assertEqual(set(bag0.variables), {'a', 'b', 'c', 'xyz'})

        # Tests for bagging with subsets of variables
        bag1 = self.db.bag(attr=('a', 'b'))
        bag2 = self.db.bag(attr={'b', 'c'})
        bag3 = self.db.bag(attr=1)
        bag4 = self.db.bag(attr=2)
        bag5 = self.db.bag(attr=4)

        self.assertEqual(len(bag1), len(self.db))
        self.assertEqual(set(bag1.variables), {'a', 'b'})

        self.assertEqual(len(bag2), len(self.db))
        self.assertEqual(set(bag2.variables), {'b', 'c'})

        self.assertEqual(len(bag3), len(self.db))
        self.assertEqual(len(bag3.variables), 1)

        self.assertEqual(len(bag4), len(self.db))
        self.assertEqual(len(bag4.variables), 2)

        self.assertEqual(len(bag5), len(self.db))
        self.assertEqual(len(bag5.variables), 4)

        # Tests for specifying the size of the bagged database
        bag6 = self.db.bag(nSamples=1)
        bag7 = self.db.bag(nSamples=len(self.db))
        bag8 = self.db.bag(nSamples=(2 * len(self.db)))

        self.assertEqual(len(bag6), 1)
        self.assertEqual(len(bag6.variables), len(self.db.variables))

        self.assertEqual(len(bag7), len(self.db))
        self.assertEqual(len(bag7.variables), len(self.db.variables))

        self.assertEqual(len(bag8), 2 * len(self.db))
        self.assertEqual(len(bag8.variables), len(self.db.variables))

        # Tests for weighted bagging
        weights = np.ones(len(self.db)) / len(self.db)
        bag9 = self.db.bag(weights=weights)
        weights = np.random.randint(0, 10, len(self.db))
        bag10 = self.db.bag(weights=weights)

        self.assertEqual(len(bag9), len(self.db))
        self.assertEqual(len(bag9.variables), len(self.db.variables))

        self.assertEqual(len(bag10), len(self.db))
        self.assertEqual(len(bag10.variables), len(self.db.variables))

    def test_bagPSD(self):
        # Tests for normal bagging
        db = database.TableDB(self.db.variables, self.db.data,
                              datatype=database.CONTINUOUS)
        bag0 = db.bagPSD()

        self.assertEqual(len(bag0), len(db))
        self.assertEqual(set(bag0.variables), {'a', 'b', 'c', 'xyz'})

        # Tests for bagging with subsets of variables
        bag1 = db.bagPSD(attr=('a', 'b'))
        bag2 = db.bagPSD(attr={'b', 'c'})
        bag4 = db.bagPSD(attr=2)
        bag5 = db.bagPSD(attr=4)

        self.assertEqual(len(bag1), len(db))
        self.assertEqual(set(bag1.variables), {'a', 'b'})

        self.assertEqual(len(bag2), len(db))
        self.assertEqual(set(bag2.variables), {'b', 'c'})

        self.assertEqual(len(bag4), len(db))
        self.assertEqual(len(bag4.variables), 2)

        self.assertEqual(len(bag5), len(db))
        self.assertEqual(len(bag5.variables), 4)

        # Tests for specifying the size of the bagged database
        bag7 = db.bagPSD(nSamples=len(db))
        bag8 = db.bagPSD(nSamples=(2 * len(db)))

        self.assertEqual(len(bag7), len(db))
        self.assertEqual(len(bag7.variables), len(db.variables))

        self.assertEqual(len(bag8), 2 * len(db))
        self.assertEqual(len(bag8.variables), len(db.variables))

        # Tests for weighted bagging
        weights = np.ones(len(db)) / len(db)
        bag9 = db.bagPSD(weights=weights)
        weights = np.random.randint(0, 10, len(db))
        bag10 = db.bagPSD(weights=weights)

        self.assertEqual(len(bag9), len(db))
        self.assertEqual(len(bag9.variables), len(db.variables))

        self.assertEqual(len(bag10), len(db))
        self.assertEqual(len(bag10.variables), len(db.variables))

    def test_sample(self):
        s1 = self.db.sample()
        s2 = self.db.sample(2)
        s3 = self.db.sample(('a', 'b', 'c'))

        self.assertEqual(len(s1), len(self.db.variables))
        self.assertEqual(len(s2), 2)
        self.assertEqual(len(s3), 3)

        # Tests for weight sampling
        weights = np.ones(len(self.db)) / len(self.db)
        s4 = self.db.sample(weights=weights)
        weights = np.random.randint(0, 10, len(self.db))
        s5 = self.db.sample(weights=weights)

        self.assertEqual(len(s4), len(self.db.variables))
        self.assertEqual(len(s5), len(self.db.variables))

    def test_partition(self):
        for nSamples in range(len(self.db) + 1):
            p1, p2 = self.db.partition(nSamples)

            self.assertEqual(len(p1), nSamples)
            self.assertEqual(len(p2), len(self.db) - nSamples)

    def test_randomPartition(self):
        for nSamples in range(len(self.db) + 1):
            nSamples = 10
            p1, p2 = self.db.randomPartition(nSamples)

            self.assertEqual(len(p1), nSamples)
            self.assertEqual(len(p2), len(self.db) - nSamples)

if __name__ == '__main__':
    unittest.main()
